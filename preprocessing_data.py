import pandas as pd
import numpy as np
import re


def col_x_val_transformer(ds:pd.DataFrame,excl_col:list=None):
    row_strg =""
    entry_1 =""
    entry_2=""
    label =""
    new_ds =[]
    cols = list(ds.columns.values)

    if excl_col == None:
        excl_col = ['xxx']

    excld_col = ['l_' + sub for sub in excl_col]
    excld_col = ['r_' + sub for sub in excl_col]
    
        

    for index, rows in ds.iterrows():
        row_strg =""
        entry_1 =""
        entry_2=""
        label =""
        for col in cols:
            if col in excld_col:
                continue
            elif col =='r_og_id':
                continue
            elif col=='r_in_candpairs':
                continue
            elif col == cols[-1]:
                label += '{}{}'.format('\t', str(rows[col]))
            elif col.find('ltable.') != -1:
                 
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_1 += col_str + val_str
            elif  col.find('rtable.') != -1:
                 
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_2 += col_str + val_str
            elif col.find('l_ltable.') != -1:
                 
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_1 += col_str + val_str
            elif  col.find('r_rtable.') != -1:
                 
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_2 += col_str + val_str
            elif col.find('ltable_') != -1:
                 
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_1 += col_str + val_str
            elif  col.find('rtable_') != -1:
                 
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_2 += col_str + val_str
            elif col.find('l_ltable_') != -1:
                 
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_1 += col_str + val_str
            elif  col.find('r_rtable_') != -1:
                 
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_2 += col_str + val_str
            elif col.find('l_og_nstr.') != -1:
                
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_1 += col_str + val_str
            elif col.find('r_nstr') != -1:
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_2 += col_str + val_str
            elif col.find('l_og.') != -1:
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_1 += col_str + val_str
            elif col.find('r_str.') != -1:
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_2 += col_str + val_str
            elif col.find('l_')!=-1:
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_1 += col_str + val_str
            elif col.find('r_') != -1:
                no_tabbed_val = ' '.join(str(rows[col]).split())
                col_str= " COL " + str(col)
                val_str= " VAL " + no_tabbed_val
                entry_1 += col_str + val_str
        

        row_strg = entry_1 + ' \t ' + entry_2 + label

        new_ds.append(row_strg) 

    return new_ds


def create_txt_file(ds, path, filename):
    destination = path+ "\\" + filename +".txt" #
    with open(destination, 'w',encoding='utf-8') as output:
        for row in ds:
            output.write(str(row) + '\n')

