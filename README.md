# pschwind_master_theisis

**Please check the paths of the scripts before running them!**

## Required installations:

- [Ditto](https://github.com/megagonlabs/ditto)
- [DeepMatcher](https://github.com/anhaidgroup/deepmatcher)
- [Magellan](https://github.com/anhaidgroup/py_entitymatching) 

To make DeepMatcher run on GPUs this might help you:
1.Step: 
pip install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html
2.Step
pip install torchtext==0.10.0.
3.Step:
pip install deepmatcher


## How to run the data generator

To install the data generator download the folder and add it to your Python environment. Might be that some changes in your system environment variables had to be done too.

To run the data generator to reproduce the steps taken in this thesis use the following scripts for the different scenarios:
Both of the scripts are in the Structured/scripts folder.
- [structured](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/Structuredness/scripts/tst_data_creation_strc.py)
- [numeric](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/Structuredness/scripts/tst_data_creationV2.py)

**WARNING: If you run the data generator different data sets will be generated! E.g. matches are sampled randomly from input!**

## How to reproduce Ditto

After data generation, Ditto needs some preprocessing. The following notebooks can be run:

In order to run them you also need the following python scripts : [preprocessing_data.py](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/preprocessing_data.py)
- structure:[preprocess_data](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/Structuredness/scripts/Ditto/preprocess_data.ipynb)
- numeric: [transform_data](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/Numeric_experiment/Ditto/transform_data.ipynb)

Remember to adapt your config file and store the generated data within Ditto.

Here you can see the scripts for [structured](https://gitlab.com/pschwind/pschwind_master_theisis/-/tree/main/Structuredness/scripts/Ditto/shell_scripts) and [numeric](https://gitlab.com/pschwind/pschwind_master_theisis/-/tree/main/Numeric_experiment/Ditto) runs.

**Adapt paths for the scripts!**

## How to reproduce DeepMatcher

After data generation, DeepMatcher needs some preprocessing. The following notebooks can be run:

- structure:[prep_dm](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/Structuredness/scripts/DeepMatcher/prep_dm.ipynb)
- numeric: [dm_prep](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/Numeric_experiment/DeepMatcher/dm_prep.ipynb)



Here you can see the scripts for [structured](https://gitlab.com/pschwind/pschwind_master_theisis/-/tree/main/Structuredness/scripts/DeepMatcher) and [numeric](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/Numeric_experiment/DeepMatcher) runs.

**Some adaptations within the scripts might be needed!**

## How to reproduce DeepMatcher

After data generation, Magellan needs some preprocessing. The following notebooks can be run:

- structure:[preproc_for_mag_v2](https://gitlab.com/pschwind/pschwind_master_theisis/-/blob/main/Structuredness/scripts/Magellan/data/preproc_for_mag_v2.ipynb)
- numeric: unzip the data folder within Numeric folder -> run mag_prep.ipynb



Here you can see the scripts for [structured](https://gitlab.com/pschwind/pschwind_master_theisis/-/tree/main/Structuredness/scripts/Magellan) and [numeric](https://gitlab.com/pschwind/pschwind_master_theisis/-/tree/main/Numeric_experiment/Magellan) runs.

**Some adaptations within the scripts might be needed!**


## Further files

Results consist of the gathered F1-scores reported in the thesis. Also, some failed experiments about Sign Test are in the excel file

Dtype of importance show some of the experiments done for the data selection process mentioned in the thesis