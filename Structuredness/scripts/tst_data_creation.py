import pandas as pd
import numpy as np

from data_generator import DataGenerator as dg
from data_generator import ManageStructure as ms
from data_generator import DataProfiling
from data_generator import utilitiy_functions as uf
import data_generator.DataCreator as dc

import py_stringmatching as sm
import py_stringsimjoin as ssj

import time
import random
import sys

from scipy.spatial.distance import cosine
from sklearn.feature_extraction.text import TfidfVectorizer

with open('Structuredness/logs/data_creationV02_stdout.txt', 'w') as sys.stdout:
    start = time.time()
    print("Start with Bike01\n")
    bike1 = dc.DataCreator('data_set/Bikes/bikedekho.csv','data_set/Bikes/bikewale.csv','data_set/Bikes/labeled_data_copy.csv',
                        cp_ltable='og',gen_bmp_join='bike_name',
                        filename='bike01', output_path='Structuredness/data/Bikes/')
    bike1.run(1000,0.7,2)
    print("Start with Bike02\n")
    bike2 = dc.DataCreator('data_set/Bikes/bikewale.csv','data_set/Bikes/bikedekho.csv','data_set/Bikes/labeled_data_copy.csv',
                        cp_ltable='gen',gen_bmp_join='bike_name',
                        filename='bike02', output_path='Structuredness/data/Bikes/')
    bike2.run(1000,0.7,2)

    print("Start with Cosmetics01\n")
    cos1 = dc.DataCreator('data_set/Cosmetics/amazon.csv','data_set/Cosmetics/sephora.csv','data_set/Cosmetics/labeled_data.csv',
                        cp_ltable='gen',gen_bmp_join='Description',filename='cosmetics01',output_path='Structuredness/data/Cosmetics/')
    cos1.run(1000,0.7,2)
    print("Start with Cosmetics02\n")
    cos2 = dc.DataCreator('data_set/Cosmetics/sephora.csv','data_set/Cosmetics/amazon.csv','data_set/Cosmetics/labeled_data.csv',
                        cp_ltable='og',gen_bmp_join='Description',filename='cosmetics02',output_path='Structuredness/data/Cosmetics/')
    cos2.run(1000,0.7,2)
    print("Start with Google\n")
    ggl = dc.DataCreator('data_set/Amazon-GoogleProducts/GoogleProducts.csv','data_set/Amazon-GoogleProducts/Amazon.csv','data_set/Amazon-GoogleProducts/ggl_aws_labeled.csv',
                        cp_ltable='gen',gen_bmp_join='name',filename='ggl',output_path='Structuredness/data/Google_AWS/')
    ggl.run(1000,0.5,3)
    print("Start with GGL_AWS\n")
    ggl_aws = dc.DataCreator('data_set/Amazon-GoogleProducts/Amazon.csv','data_set/Amazon-GoogleProducts/GoogleProducts.csv','data_set/Amazon-GoogleProducts/ggl_aws_labeled.csv',
                        cp_ltable='og',gen_bmp_join='name',filename='ggl_aws',output_path='Structuredness/data/Google_AWS/')
    ggl_aws.run(1000,0.5,3)
    print("Start with Walmart\n")
    wal = dc.DataCreator('data_set/Products_Wallmart_AWS/walmart.csv','data_set/Products_Wallmart_AWS/amazon.csv','data_set/Products_Wallmart_AWS/wal_aws_labeled.csv',
                        cp_ltable='og',gen_bmp_join='title',filename='wal',output_path='Structuredness/data/Walmart_AWS/')
    wal.run(1000,0.5,3)
    print("Start with WAL_AWS\n")
    wal_aws = dc.DataCreator('data_set/Products_Wallmart_AWS/amazon.csv','data_set/Products_Wallmart_AWS/walmart.csv','data_set/Products_Wallmart_AWS/wal_aws_labeled.csv',
                            cp_ltable='gen',gen_bmp_join='title',filename='wal_aws',output_path='Structuredness/data/Walmart_AWS/')
    wal_aws.run(1000,0.5,3)

    print("Runtime was {} secs!".format(str(time.time()-start)))