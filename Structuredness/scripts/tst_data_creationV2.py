import pandas as pd
import numpy as np

from data_generator import DataGenerator as dg
from data_generator import ManageStructure as ms
from data_generator import DataProfiling
from data_generator import utilitiy_functions as uf
import py_stringmatching as sm
import py_stringsimjoin as ssj

import time
import random
import sys

from scipy.spatial.distance import cosine
from sklearn.feature_extraction.text import TfidfVectorizer

with open('Structuredness/logs/data_creationV07_num_stdout.txt', 'w') as sys.stdout:
        #'Structuredness/logs/data_creationV07_stdout.txt'
    """bike01 =dg.DataGenerator('data_set/Bikes/bikedekho.csv','data_set/Bikes/bikewale.csv','data_set/Bikes/labeled_data_copy.csv',
                        gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='bike_name')
    bike01.run('bike01','Structuredness/data/Bikes/',1000,cosine_threshold=0.1,nm_factor=3,
                match_multiplier=30,negatives_multiplier=30,numeric=False)

    bike02 = dg.DataGenerator('data_set/Bikes/bikewale.csv','data_set/Bikes/bikedekho.csv','data_set/Bikes/labeled_data_copy.csv',
                            gen_from_cp_reference='rtable_id',bmp_cp_reference='ltable_id',gen_bmp_join='bike_name')
    bike02.run('bike02','Structuredness/data/Bikes/',1000,cosine_threshold=0.1,nm_factor=3,
                match_multiplier=30,negatives_multiplier=30,numeric=False)

    cos01 =dg.DataGenerator('data_set/Cosmetics/amazon.csv','data_set/Cosmetics/sephora.csv','data_set/Cosmetics/labeled_data.csv',
                            gen_from_cp_reference='rtable_id',bmp_cp_reference='ltable_id',gen_bmp_join='Description')
    cos01.run('cosmetics01','Structuredness/data/Cosmetics/',1500,cosine_threshold=0.1,nm_factor=3,
                match_multiplier=20,negatives_multiplier=20,numeric=False)

    cos02 =dg.DataGenerator('data_set/Cosmetics/sephora.csv','data_set/Cosmetics/amazon.csv','data_set/Cosmetics/labeled_data.csv',
                            gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='Description')
    cos02.run('cosmetics02','Structuredness/data/Cosmetics/',1500,cosine_threshold=0.1,nm_factor=3,
                match_multiplier=20,negatives_multiplier=20,numeric=False)

    ggl = dg.DataGenerator('data_set/Amazon-GoogleProducts/GoogleProducts_id_enc.csv','data_set/Amazon-GoogleProducts/Amazon_id_enc.csv','data_set/Amazon-GoogleProducts/ggl_aws_labeled.csv',
                           gen_from_cp_reference='rtable_id',bmp_cp_reference='ltable_id',gen_bmp_join='name')
    ggl.run('ggl','Structuredness/data/Google_AWS/',1500,0.1,nm_factor=3,
            match_multiplier=20,negatives_multiplier=20,numeric=False)

    ggl_aws = dg.DataGenerator('data_set/Amazon-GoogleProducts/Amazon_id_enc.csv','data_set/Amazon-GoogleProducts/GoogleProducts_id_enc.csv','data_set/Amazon-GoogleProducts/ggl_aws_labeled.csv',
                           gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='name')
    ggl_aws.run('ggl_aws','Structuredness/data/Google_AWS/',1000,0.1, nm_factor=3,
                match_multiplier=30,negatives_multiplier=30,numeric=False)

    wal = dg.DataGenerator('data_set/Products_Wallmart_AWS/walmart.csv','data_set/Products_Wallmart_AWS/amazon.csv','data_set/Products_Wallmart_AWS/wal_aws_labeled.csv',
                           gen_from_cp_reference='rtable_id',bmp_cp_reference='ltable_id',gen_bmp_join='title')
    wal.run('wal','Structuredness/data/Walmart_AWS/',1000,cosine_threshold=0.1, nm_factor=3,
            match_multiplier=30,negatives_multiplier=30,numeric=False)
    wal_aws = dg.DataGenerator('data_set/Products_Wallmart_AWS/amazon.csv','data_set/Products_Wallmart_AWS/walmart.csv','data_set/Products_Wallmart_AWS/wal_aws_labeled.csv',
                           gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='title')
    wal_aws.run('wal_aws','Structuredness/data/Walmart_AWS/',1000,cosine_threshold=0.1,nm_factor=3,
                match_multiplier=30,negatives_multiplier=30,numeric=False) 
	"""
    print("-------------------------------------------------------------------------------------------------")
    print("Numeric generation")

    bike_num =dg.DataGenerator('data_set/Bikes/bikedekho.csv','data_set/Bikes/bikewale.csv','data_set/Bikes/labeled_data_copy.csv',
                        gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='bike_name',)
    bike_num.run('bike','Numeric_experiment/data/Bikes/',3000,nm_factor=3,nm_severity=0.3, nm_percent=0.2,
                match_multiplier=3,negatives_multiplier=3,numeric=True)

    cos_num =dg.DataGenerator('data_set/Cosmetics/amazon.csv','data_set/Cosmetics/sephora.csv','data_set/Cosmetics/labeled_data.csv',
                            gen_from_cp_reference='rtable_id',bmp_cp_reference='ltable_id',gen_bmp_join='Description')
    cos_num.run('cosmetics','Numeric_experiment/data/Cosmetics/',5000,nm_factor=3,nm_severity=0.3, nm_percent=0.2,
                match_multiplier=3,negatives_multiplier=3,numeric=True)

    gglaws_num = dg.DataGenerator('data_set/Amazon-GoogleProducts/Amazon_id_enc.csv','data_set/Amazon-GoogleProducts/GoogleProducts_id_enc.csv','data_set/Amazon-GoogleProducts/ggl_aws_labeled.csv',
                           gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='name')
    gglaws_num.run('gglaws','Numeric_experiment/data/Google_AWS/',1000,nm_factor=3,nm_severity=0.3, nm_percent=0.2,
                    match_multiplier=3,negatives_multiplier=3,numeric=True)

    walaws_num = dg.DataGenerator('data_set/Products_Wallmart_AWS/walmart.csv','data_set/Products_Wallmart_AWS/amazon.csv','data_set/Products_Wallmart_AWS/wal_aws_labeled.csv',
                           gen_from_cp_reference='rtable_id',bmp_cp_reference='ltable_id',gen_bmp_join='title')
    walaws_num.run('walaws','Numeric_experiment/data/Walmart_AWS/',1500,nm_factor=3,nm_severity=0.3, nm_percent=0.2,
                    match_multiplier=3,negatives_multiplier=3,numeric=True)




