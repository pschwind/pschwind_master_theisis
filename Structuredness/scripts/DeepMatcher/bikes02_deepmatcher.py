"""
This file is used to test out the experiment setup of this thesis.

Expectations:
- structure manipulation of data sets works
- manipulation of numeric attribute decisiveness works
- entity matching with dataset does make sense

Test scenarios:
- Entity Matching with a high structured data sets
- Entity Matching with no structured data sets
- Entity Matching with numeric attribute decisiveness
- Entity Matching with string attribute dominant matching

Current focus on DeepMatcher

"""

#----------------------------------------------------------------------------------------
#
#
# General EM-process:
# 1. load and generate data
# 2. preprocess data
# 3. Blocking 
# 4. Create candidate pairs
# 5. Create goldstandard
# 5. Create train, val, test split
# 6. Learn and apply model
# 7. save and log results of model
#
#
#---------------------------------------------------------------------------------------

import pandas as pd
import numpy as np
import sys
import os
import random
import time

import deepmatcher as dm


with open('Structuredness/logs/Bikes/V2_bikes02_stdout.txt', 'w') as sys.stdout:
    start = time.time()
    #DeepMatcher for structure

    if os.path.exists('/ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/data/Bikes/cacheddata.pth'):
        os.remove('/ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/data/Bikes/cacheddata.pth')

    # load data via DeepMatcher
    print("\n Start DeepMatcher for structure \n")
    train, validation, test = dm.data.process(
        path='Structuredness/scripts/DeepMatcher/data/Bikes/',
        train='bike02_train_strc.csv', 
        validation='bike02_validate_strc.csv',
        test='bike02_test_strc.csv',
        ignore_columns=('index','l_id','l_url','r_id','r_url','r_in_candpairs','r_og_id'),
        left_prefix='l_',
        right_prefix='r_',
        label_attr ='gold',
        id_attr='_id')
    
    
    summarizer =['sif', 'rnn', 'attention','hybrid']
    for sum in summarizer:

        model = dm.MatchingModel(attr_summarizer=sum)

        #model.initialize(train)
        model.run_train(
            train,
            validation,
            epochs=25,
            learning_rate=1e-3,
            batch_size=32,
            best_save_path='Structuredness/logs/Bikes/bikes02_best_save_path.pth')

        pred = model.run_eval(test)

        print("Best F1-score of {} during test was: {}".format(sum, pred))

    #delete cachedata.pth so it does not f up the script
    print("\n DeepMatcher ended for structure \n")
    print("\n Delete chachedata.pth for unstructured run ")
    if os.path.exists('/ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/data/Bikes/cacheddata.pth'):
        os.remove('/ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/data/Bikes/cacheddata.pth')
          
   # unstructured run
    print("\n Start DeepMatcher for no structure \n")
    train_ns, validation_ns, test_ns = dm.data.process(
        path='Structuredness/scripts/DeepMatcher/data/Bikes/',
        train='bike02_train_nstrc.csv', 
        validation='bike02_validate_nstrc.csv',
        test='bike02_test_nstrc.csv',
        ignore_columns=('index','l_id','l_url','r_id','r_url','r_in_candpairs','r_og_id'),
        left_prefix='l_',
        right_prefix='r_',
        label_attr ='gold',
        id_attr='_id')

    #TODO: Understand the mistake here seems that there is some prefix issue. I think!
    #ValueError: Attribute l_og_nstr.id is not a left or a right table column, not a label or id and is not ignored. Not sure what it is...

    summarizer =['sif', 'rnn', 'attention','hybrid']
    for sum in summarizer:

        model = dm.MatchingModel(attr_summarizer=sum)

        #model.initialize(train)
        model.run_train(
            train_ns,
            validation_ns,
            epochs=25,
            learning_rate=1e-3,
            batch_size=32,
            best_save_path='Structuredness/logs/Bikes/bikes02_best_save_path_nstrc.pth')

        pred = model.run_eval(test_ns)

        print("Best F1-score of {} during test was: {}".format(sum, pred))
    print("DeepMatcher ended for no structure \n") 
    print("DeepMatcher took {} seconds to finish".format(str(time.time()-start)))
