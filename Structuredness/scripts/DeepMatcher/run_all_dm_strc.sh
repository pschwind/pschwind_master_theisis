#!/bin/bash

echo "start DeepMatcher run for structure"

echo "Start Bike runs"
python /ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/bikes01_deepmatcher.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/bikes02_deepmatcher.py

echo ""
echo "Start Cosmetics run"
python /ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/cosmetic01_deepmatcher.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/cosmetic02_deepmatcher.py

echo ""
echo "Start Google runs"
python /ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/google_deepmatcher.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/ggl_aws_deepmatcher.py

echo ""
echo "Start Walmart runs"
python /ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/walmart_deepmatcher.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/DeepMatcher/wal_aws_deepmatcher.py

echo "Run ended"
