import pandas as pd
import numpy as np

import py_stringsimjoin as ssj
import py_stringmatching as sm

import time

strc = pd.read_csv('Structuredness/data/Bikes/bike01_struct.csv')
og = pd.read_csv('data_set/Bikes/bikedekho.csv')

og=og.add_prefix('og_')
strc.columns = strc.columns.str.replace(".","_")

ws = sm.WhitespaceTokenizer()

op = ssj.cosine_join(og, strc,'og_id','str_id', 'og_bike_name','str_bike_name', ws , 0.5,
                        l_out_attrs=['og_id','og_bike_name','og_city_posted','og_km_driven','og_color',
                                    'og_fuel_type','og_price','og_model_year','og_owner_type','og_url'],
                        r_out_attrs=['str_id','str_bike_name','str_city_posted','str_km_driven','str_color',
                                    'str_fuel_type','str_price','str_model_year','str_owner_type','str_url'])
"""
for index, rows in op.iterrows():
        bike_id= rows['l_og.id']
        strc_id = rows['r_str.id']

        for idx, r in strc.iterrows():
            
            if r['str.og_id'] == bike_id and r['str.id']==strc_id:
                op.at[index, '_sim_score'] = 1 
                break
            else:
                op.at[index, '_sim_score'] = 0
"""
op.rename(columns={'_sim_score':'sim_score'},inplace=True)
start = time.time()

for row in op.itertuples():
    bike_id = row.l_og_id
    strc_id = row.r_str_id

    for r in strc.itertuples():
        if r.str_og_id == bike_id and r.str_id == strc_id:
            op['sim_score'].at[row.Index] = 1
            break
        else:
            op['sim_score'].at[row.Index] = 0

print(op['sim_score'].value_counts())
print("Itertuples took {} seconds".format(str(time.time()-start)))


filter1 = op['l_og_id'] == strc['str_og_id']
filter2 = op['r_str_id']==strc['str_id']

test = op.where(filter1 & filter2,)