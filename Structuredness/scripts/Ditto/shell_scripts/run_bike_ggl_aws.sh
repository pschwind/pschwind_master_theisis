#! /bin/bash

echo "transformer run for bike01 starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_bike01_structure.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_bike01_no_structure.sh
echo "transformer run for bike01 finished"
echo ""
echo ""


echo "transformer run for bike02 starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_bike02_structure.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_bike02_no_structure.sh
echo "transformer run for bike02 finished"
echo ""
echo ""

echo "transformer run for google starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_google_structure.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_google_no_structure.sh
echo "transformer run for google finished"
echo ""
echo ""

echo "transformer run for ggl_aws starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_ggl_aws_structure.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_ggl_aws_no_structure.sh
echo "transformer run for ggl_aws finished"