#! /bin/bash
exec > /ceph/pschwind/datagenerator/Structuredness/logs/Cosmetics/ditto_cosmetics02_structure.txt 2>&1
python train_ditto.py \
  --task experiment/cos02_strc\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --save_model 