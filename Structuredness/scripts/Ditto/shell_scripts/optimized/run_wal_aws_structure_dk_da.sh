#! /bin/sh
exec > /ceph/pschwind/datagenerator/Structuredness/logs/Walmart_AWS/ditto_optimized_wal_aws_structure.txt 2>&1
echo "Domain Knowledge"
echo ""
python train_ditto.py \
  --task experiment/wal_aws_strc\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --dk product \
  --save_model 

echo ""
echo "Data Augementation"
echo ""
python train_ditto.py \
  --task experiment/wal_aws_strc\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --da all \
  --save_model 

echo ""
echo "Summarization"
echo ""
python train_ditto.py \
  --task experiment/wal_aws_strc\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --summarize \
  --save_model 