#! /bin/bash
exec > /ceph/pschwind/datagenerator/Structuredness/logs/Cosmetics/ditto_optimized_cosmetics02_no_structure.txt 2>&1
echo ""
echo "Domain Knowledge"
echo ""
python train_ditto.py \
  --task experiment/cos02_no_strc\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --dk product\
  --save_model

echo ""
echo "Data Augmentation"
echo ""
python train_ditto.py \
  --task experiment/cos02_no_strc\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --da all\
  --save_model

echo ""
echo "Summarizer"
echo ""
python train_ditto.py \
  --task experiment/cos02_no_strc\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --summarize \
  --save_model