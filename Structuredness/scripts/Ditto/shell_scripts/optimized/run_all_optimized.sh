#! /bin/bash

exec &> ditto_optimized_all.txt

echo "transformer run for bike01 starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_bike01_structure_dk_da.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_bike01_no_structure_dk_da.sh

echo "transformer run for bike01 finished"
echo ""
echo ""


echo "transformer run for bike02 starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_bike02_structure_dk_da.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_bike02_no_structure_dk_da.sh

echo "transformer run for bike02 finished"
echo ""
echo ""

echo "transformer run for google starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_google_structure_dk_da.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_google_no_structure_dk_da.sh

echo "transformer run for google finished"
echo ""
echo ""

echo "transformer run for ggl_aws starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_ggl_aws_structure_dk_da.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_ggl_aws_no_structure_dk_da.sh


echo "transformer run for ggl_aws finished"

echo "transformer run for cosmetic01 starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_cosmetics01_structure_dk_da.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_cosmetics01_no_structure_dk_da.sh


echo "transformer run for cosmetic01 finished"
echo ""
echo ""


echo "transformer run for cosmetic02 starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_cosmetics02_structure_dk_da.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_cosmetics02_no_structure_dk_da.sh

echo "transformer run for cosmetic02 finished"
echo ""
echo ""

echo "transformer run for walmart starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_walmart_structure_dk_da.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_walmart_no_structure_dk_da.sh

echo "transformer run for walmart finished"
echo ""
echo ""

echo "transformer run for wal_aws starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_wal_aws_structure_dk_da.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/optimized/run_wal_aws_structure_dk_da.sh

echo "transformer run for wal_aws finished"