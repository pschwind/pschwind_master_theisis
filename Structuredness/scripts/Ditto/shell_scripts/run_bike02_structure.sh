#!/bin/bash
exec > /ceph/pschwind/datagenerator/Structuredness/logs/Bikes/ditto_bike02_structure.txt 2>&1
python train_ditto.py \
  --task experiment/bike02_strc\
  --logdir Structuredness/log/Bikes/ \
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --save_model 