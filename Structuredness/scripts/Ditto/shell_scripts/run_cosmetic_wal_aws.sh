#! /bin/bash

echo "transformer run for cosmetic01 starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_cosmetics01_structure.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_cosmetics01_no_structure.sh
echo "transformer run for cosmetic01 finished"
echo ""
echo ""


echo "transformer run for cosmetic02 starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_cosmetics02_structure.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_cosmetics02_no_structure.sh
echo "transformer run for cosmetic02 finished"
echo ""
echo ""

echo "transformer run for walmart starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_walmart_structure.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_walmart_no_structure.sh
echo "transformer run for walmart finished"
echo ""
echo ""

echo "transformer run for wal_aws starts"
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_wal_aws_structure.sh
sh /ceph/pschwind/datagenerator/Structuredness/scripts/Ditto/shell_scripts/run_wal_aws_no_structure.sh
echo "transformer run for wal_aws finished"