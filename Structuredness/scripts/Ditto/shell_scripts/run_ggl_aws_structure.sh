#! /bin/bash
exec > /ceph/pschwind/datagenerator/Structuredness/logs/Google_AWS/ditto_ggl_aws_structure.txt 2>&1

python train_ditto.py \
  --task experiment/ggl_aws_strc\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --save_model 