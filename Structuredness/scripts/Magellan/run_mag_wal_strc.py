"""
Run experiments with the magellan pipeline for structuredness

Learning:
 - need to have same column names, some how preprocessing and storing for Magellan would be dope
"""

import pandas as pd
import numpy as np

import py_entitymatching as em
import py_stringmatching as sm
import py_stringsimjoin as ssj

import sys

#TODO: logging into txt file
with open('Structuredness/logs/Magellan/V2_mag_wal_strc_stdout.txt', 'w') as sys.stdout:

        og = em.read_csv_metadata('Structuredness/scripts/Magellan/data/Walmart_AWS/v2_mag_wal_og.csv', key='id')
        strc = em.read_csv_metadata('Structuredness/scripts/Magellan/data/Walmart_AWS/v2_mag_wal_strc.csv',key='id')

        
        # maybe prefix here has to get rid of as well
        op_labeled = em.read_csv_metadata('Structuredness/scripts/Magellan/data/Walmart_AWS/v2_mag_op_wal_strc.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')


        test = em.read_csv_metadata('Structuredness/data/Walmart_AWS/wal_test_strc.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')
        train = em.read_csv_metadata('Structuredness/data/Walmart_AWS/wal_train_strc.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')

        dt = em.DTMatcher(name='DecisionTree', random_state=0)
        svm = em.SVMMatcher(name='SVM', random_state=0)
        rf = em.RFMatcher(name='RF', random_state=0)
        lg = em.LogRegMatcher(name='LogReg', random_state=0)
        ln = em.LinRegMatcher(name='LinReg')

        feature = em.get_features_for_matching(og,strc,validate_inferred_attr_types=False)

        print(feature.feature_name)

        test_set = em.extract_feature_vecs(train, feature_table=feature, attrs_after='gold',show_progress=False)

        check= test_set[test_set['gold']==1]

        excld_vecs =[col for col in test_set.columns if 'id' in col]
        idx_vecs = [col for col in test_set.columns if 'index' in col]
        modn_vecs = [col for col in test_set.columns if 'modelno' in col]
        upc_vecs = [col for col in test_set.columns if 'upc' in col]
        iurl_vecs = [col for col in test_set.columns if 'imageurl' in col]

        excld_vecs.extend(idx_vecs)
        excld_vecs.extend(modn_vecs)
        excld_vecs.extend(upc_vecs)
        excld_vecs.extend(iurl_vecs)

        check.drop(columns=excld_vecs,axis=1,inplace=True) 
        print(check)

        print("Is imputation needed? {}".format(any(pd.notnull(test_set))))

        #SimpleImputer does not accept missing values encoded as NaN natively.
        if any(pd.notnull(test_set)):
                test_set=em.impute_table(test_set, 
                        exclude_attrs=['_id', 'l_id', 'r_id', 'gold'],
                        missing_val= -1,
                        strategy='mean',
                        val_all_nans=0)

        result = em.select_matcher([dt, rf, svm, ln, lg], table=test_set, 
                exclude_attrs=excld_vecs,
                k=5,
                target_attr='gold', metric_to_select_matcher='f1', random_state=0)
        print(result['cv_stats'])

