"""
Run experiments with the magellan pipeline for structuredness

Learning:
 - need to have same column names, some how preprocessing and storing for Magellan would be dope
"""

import pandas as pd
import numpy as np

import py_entitymatching as em
import py_stringmatching as sm
import py_stringsimjoin as ssj

import sys

#TODO: logging into txt file
with open('Structuredness/logs/Magellan/V2_mag_cosmetic01_no_strc_debug.txt', 'w') as sys.stdout:

        og = em.read_csv_metadata('Structuredness/data/Cosmetics/cosmetics01_og_nstrc.csv', key='id')
        strc = em.read_csv_metadata('Structuredness/data/Cosmetics/cosmetics01_gen_nstrc.csv',key='id')

        
        # maybe prefix here has to get rid of as well
        op_labeled = em.read_csv_metadata('Structuredness/data/Cosmetics/cosmetics01_op_nstrc.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')

        train =em.read_csv_metadata('Structuredness/data/Cosmetics/cosmetics01_train_nstrc.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')
        test = em.read_csv_metadata('Structuredness/data/Cosmetics/cosmetics01_test_nstrc.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')

        dt = em.DTMatcher(name='DecisionTree', random_state=0)
        svm = em.SVMMatcher(name='SVM', random_state=0)
        rf = em.RFMatcher(name='RF', random_state=0)
        lg = em.LogRegMatcher(name='LogReg', random_state=0)
        ln = em.LinRegMatcher(name='LinReg')


        atypes_og = em.get_attr_types(og)
        atypes_strc = em.get_attr_types(strc)

        corr = em.get_attr_corres(og,strc)
        tok = em.get_tokenizers_for_matching()
        sim =em.get_sim_funs_for_matching()

        cat = em.get_catalog()
        print(cat.keys())

        print(atypes_og.keys())
        print(atypes_strc.keys())

        print(corr['corres'])
        print(tok.keys())
        print(sim.keys())

        feature = em.get_features_for_matching(og,strc,validate_inferred_attr_types=True)

        feature_table = em.get_features(og, strc, atypes_og, atypes_strc, corr, tok, sim)
        #print(feature_table.keys())




"""
        feature = em.get_features_for_matching(og,strc,validate_inferred_attr_types=False)

        print(feature.feature_name)

        test_set = em.extract_feature_vecs(train, feature_table=feature, attrs_after='gold',show_progress=False)

        print("Is imputation needed? {}".format(any(pd.notnull(test_set))))

        #SimpleImputer does not accept missing values encoded as NaN natively.
        if any(pd.notnull(test_set)):
                test_set=em.impute_table(test_set, 
                        exclude_attrs=['_id', 'l_id', 'r_id', 'gold'],
                        missing_val= -1,
                        strategy='mean',
                        val_all_nans=0)

        result = em.select_matcher([dt, rf, svm, ln, lg], table=test_set, 
                exclude_attrs=['_id', 'l_id', 'r_id', 'gold'],
                k=5,
                target_attr='gold', metric_to_select_matcher='f1', random_state=0)
        print(result['cv_stats'])

"""