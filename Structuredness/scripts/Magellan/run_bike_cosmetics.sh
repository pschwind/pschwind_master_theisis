#! /bin/bash

echo "Start run of Bikes"

python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_bike01_strc.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_bike01_no_strc.py

python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_bike02_strc.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_bike02_no_strc.py

echo "End of run Bikes"

echo "Start cosmetics run"
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_cos01_strc.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_cos01_no_strc.py

python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_cos02_strc.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_cos02_no_strc.py
echo "Cosmetics run ended"