#! /bin/bash

echo "Start run of Google"

python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_ggl_strc.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_ggl_no_strc.py

python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_gglaws_strc.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_gglaws_no_strc.py

echo "End of run Google"

echo "Start walmart run"
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_wal_strc.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_wal_no_strc.py

python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_walaws_strc.py
python /ceph/pschwind/datagenerator/Structuredness/scripts/Magellan/run_mag_walaws_no_strc.py
echo "Walmart run ended"