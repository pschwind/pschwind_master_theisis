import pandas as pd
import numpy as np

from data_generator import DataGenerator as dg
from data_generator import ManageStructure as ms

import py_stringmatching as sm
import py_stringsimjoin as ssj

import time
import random
import sys

with open('Structuredness/logs/data_creation_stdout.txt', 'w') as sys.stdout:
    start = time.time() 
    #------------------------------------------------------------------------------
    # 
    #  Data creation for BIKES!
    # 
    # ----------------------------------------------------------------------------  

    print("\n Start Bike creation! \n")

    bike,bike_og_no_strc,bike_strc,bike_no_strc=dg.generate_data_sets(input_path='data_set/Bikes/bikedekho.csv',
                                                                    file_name='bikes01',
                                                                    output_path='Structuredness/data/Bikes/',
                                                                    entity=500)
    
    print(ssj.profile_table_for_join(bike))
    print(ssj.profile_table_for_join(bike_strc))
    print(ssj.profile_table_for_join(bike_no_strc))
    print(ssj.profile_table_for_join(bike_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    bike = bike.astype('object')
    bike_strc = bike_strc.astype('object')
    bike_no_strc = bike_no_strc.astype('object')
    bike_og_no_strc = bike_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    print("\n Create structured outputpairs for Bikes01 \n")
    op_bike_strc = ssj.cosine_join(bike, bike_strc,'og_id','str_id', 'og_bike_name','str_bike_name', ws , 0.7,
                                    l_out_attrs=['og_id','og_bike_name','og_city_posted','og_km_driven','og_color',
                                                'og_fuel_type','og_price','og_model_year','og_owner_type','og_url'],
                                    r_out_attrs=['str_id','str_bike_name','str_city_posted','str_km_driven','str_color',
                                                'str_fuel_type','str_price','str_model_year','str_owner_type','str_url'],
                                    show_progress=True)


    #select the right matches for structure
    #faster method: but seems not yet to work correctly

    op_bike_strc = dg.labeling(op_bike_strc,bike_strc,
                                ds_og_id='l_og_id',ds_r_id='r_str_id',
                                strc_id='str_id',strc_og_id='str_og_id')

    op_bike_strc.to_csv('Structuredness/data/Bikes/outputpairs_bike01_strc.csv')


    #split set into train, validate and test for structure
    train_bike_strc, validate_bike_strc, test_bike_strc = \
                np.split(op_bike_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_bike_strc)), int(.8*len(op_bike_strc))])


    # make them to csv so DeepMatcher can use them

    train_bike_strc.to_csv('Structuredness/data/Bikes//train_bike01_strc.csv')
    validate_bike_strc.to_csv('Structuredness/data/Bikes/validate_bike01_strc.csv')
    test_bike_strc.to_csv('Structuredness/data/Bikes/test_bike01_strc.csv')


    # no structure run

    print("\n Create no structured outputpairs for Bikes01 \n")
    op_bike_no_strc = ssj.cosine_join(bike_og_no_strc, bike_no_strc,'og_nstr_id','nstr_id', bike_og_no_strc.columns[-1],bike_no_strc.columns[-1], ws , 0.7,
                                    l_out_attrs=['og_nstr_id',bike_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr_id',bike_no_strc.columns[-1]],
                                    show_progress=True)

    #create gs for no_struct_aws (should be able to copy and adapt the one of structured)

    #faster method:
    op_bike_no_strc = dg.labeling(op_bike_no_strc,bike_no_strc,
                                ds_og_id='l_og_nstr_id', ds_r_id='r_nstr_id',
                                strc_id='nstr_id',strc_og_id='nstr_og_id')

    op_bike_no_strc.to_csv('Structuredness/data/Bikes/outputpairs_bike01_no_strc.csv')

    #split no struct aws into train, validation and test set
    train_bike_nstrc, validate_bike_nstrc, test_bike_nstrc = \
                np.split(op_bike_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_bike_no_strc)), int(.8*len(op_bike_no_strc))])

    train_bike_nstrc.to_csv('Structuredness/data/Bikes/train_bike01_nstrc.csv')
    validate_bike_nstrc.to_csv('Structuredness/data/Bikes/validate_bike01_nstrc.csv')
    test_bike_nstrc.to_csv('Structuredness/data/Bikes/test_bike01_nstrc.csv')
   

    bike,bike_og_no_strc,bike_strc,bike_no_strc=dg.generate_data_sets(input_path='data_set/Bikes/bikewale.csv',
                                                                    file_name='bikes02',
                                                                    output_path='Structuredness/data/Bikes/',
                                                                    entity=500)

    print(ssj.profile_table_for_join(bike))
    print(ssj.profile_table_for_join(bike_strc))
    print(ssj.profile_table_for_join(bike_no_strc))
    print(ssj.profile_table_for_join(bike_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    bike = bike.astype('object')
    bike_strc = bike_strc.astype('object')
    bike_no_strc = bike_no_strc.astype('object')
    bike_og_no_strc = bike_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    #unfortunately does not have a ignore column area and one is not able to join with the og_id
    #TODO: is this already blocking??

    print("\n Create structured outputpairs for Bikes02 \n")
    op_bike_strc = ssj.cosine_join(bike, bike_strc,'og_id','str_id', 'og_bike_name','str_bike_name', ws , 0.7,
                                    l_out_attrs=['og_id','og_bike_name','og_city_posted','og_km_driven','og_color',
                                                'og_fuel_type','og_price','og_model_year','og_owner_type','og_url'],
                                    r_out_attrs=['str_id','str_bike_name','str_city_posted','str_km_driven','str_color',
                                                'str_fuel_type','str_price','str_model_year','str_owner_type','str_url'],
                                    show_progress=True)


    #labeling 
    op_bike_strc = dg.labeling(op_bike_strc,bike_strc,
                                ds_og_id='l_og_id',ds_r_id='r_str_id',
                                strc_id='str_id',strc_og_id='str_og_id')
    op_bike_strc.to_csv('Structuredness/data/Bikes/outputpairs_bike02_strc.csv')    

    #split set into train, validate and test for structure
    train_bike_strc, validate_bike_strc, test_bike_strc = \
                np.split(op_bike_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_bike_strc)), int(.8*len(op_bike_strc))])

    
    # make them to csv so DeepMatcher can use them

    train_bike_strc.to_csv('Structuredness/data/Bikes//train_bikes02_strc.csv')
    validate_bike_strc.to_csv('Structuredness/data/Bikes/validate_bikes02_strc.csv')
    test_bike_strc.to_csv('Structuredness/data/Bikes/test_bikes02_strc.csv')


    # no structure run

    print("\n Create no structured outputpairs for Bikes02 \n")
    op_bike_no_strc = ssj.cosine_join(bike_og_no_strc, bike_no_strc,'og_nstr_id','nstr_id', bike_og_no_strc.columns[-1],bike_no_strc.columns[-1], ws , 0.7,
                                    l_out_attrs=['og_nstr_id',bike_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr_id',bike_no_strc.columns[-1]],
                                    show_progress=True)

    #labeling part
    op_bike_no_strc = dg.labeling(op_bike_no_strc,bike_no_strc,
                                ds_og_id='l_og_nstr_id', ds_r_id='r_nstr_id',
                                strc_id='nstr_id',strc_og_id='nstr_og_id')
    
    op_bike_no_strc.to_csv('Structuredness/data/Bikes/outputpairs_bikes02_no_strc.csv')

    #split no struct aws into train, validation and test set
    train_bike_nstrc, validate_bike_nstrc, test_bike_nstrc = \
                np.split(op_bike_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_bike_no_strc)), int(.8*len(op_bike_no_strc))])

    train_bike_nstrc.to_csv('Structuredness/data/Bikes/train_bikes02_nstrc.csv')
    validate_bike_nstrc.to_csv('Structuredness/data/Bikes/validate_bikes02_nstrc.csv')
    test_bike_nstrc.to_csv('Structuredness/data/Bikes/test_bikes02_nstrc.csv')


    #-----------------------------------------------------------------------------
    # 
    #  Data creation for COSMETICS!
    # 
    # ----------------------------------------------------------------------------
"""
    cos,cos_og_no_strc,cos_strc,cos_no_strc = dg.generate_data_sets(input_path='data_set/Cosmetics/amazon.csv',file_name='cos01',
                                                                    output_path='Structuredness/data/Cosmetics/',
                                                                    entity=1000)
    
    print(ssj.profile_table_for_join(cos))
    print(ssj.profile_table_for_join(cos_strc))
    print(ssj.profile_table_for_join(cos_no_strc))
    print(ssj.profile_table_for_join(cos_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    cos = cos.astype('object')
    cos_strc = cos_strc.astype('object')
    cos_no_strc = cos_no_strc.astype('object')
    cos_og_no_strc = cos_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    print("\n Create structured outputpairs cos01 \n")
    op_cos_strc = ssj.cosine_join(cos, cos_strc,'og_id','str_id', 'og_Description','str_Description', ws , 0.5,
                                    l_out_attrs=['og_id','og_Price','og_Color','og_Description'],
                                    r_out_attrs=['str_id','str_Price','str_Color','str_Description'],
                                    show_progress=True)
    
    op_cos_strc = dg.labeling(op_cos_strc,cos_strc,
                                ds_og_id='l_og_id',ds_r_id='r_str_id',
                                strc_id='str_id', strc_og_id='str_og_id')
    
    op_cos_strc.to_csv('Structuredness/data/Cosmetics/outputpairs_cos01_strc.csv')

    #split set into train, validate and test for structure
    train_cos_strc, validate_cos_strc, test_cos_strc = \
                np.split(op_cos_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_cos_strc)), int(.8*len(op_cos_strc))])

    # make them to csv so DeepMatcher can use them
    train_cos_strc.to_csv('Structuredness/data/Cosmetics//train_cos01_strc.csv')
    validate_cos_strc.to_csv('Structuredness/data/Cosmetics/validate_cos01_strc.csv')
    test_cos_strc.to_csv('Structuredness/data/Cosmetics/test_cos01_strc.csv')

    #no struct run
    print("\n Create no structured outputpairs for cos01 \n")
    op_cos_no_strc = ssj.cosine_join(cos_og_no_strc, cos_no_strc,'og_nstr_id','nstr_id', cos_og_no_strc.columns[-1],cos_no_strc.columns[-1], ws , 0.5,
                                    l_out_attrs=['og_nstr_id',cos_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr_id',cos_no_strc.columns[-1]],
                                    show_progress=True)
    
    op_cos_no_strc = dg.labeling(op_cos_no_strc,cos_no_strc,
                                ds_og_id='l_og_nstr_id',ds_r_id='r_nstr_id',
                                strc_id='nstr_id', strc_og_id='nstr_og_id')
    
    op_cos_no_strc.to_csv('Structuredness/data/Cosmetics/outputpairs_cos01_no_strc.csv')

    #split no struct aws into train, validation and test set
    train_cos_nstrc, validate_cos_nstrc, test_cos_nstrc = \
                np.split(op_cos_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_cos_no_strc)), int(.8*len(op_cos_no_strc))])

    train_cos_nstrc.to_csv('Structuredness/data/Cosmetics/train_cos01_nstrc.csv')
    validate_cos_nstrc.to_csv('Structuredness/data/Cosmetics/validate_cos01_nstrc.csv')
    test_cos_nstrc.to_csv('Structuredness/data/Cosmetics/test_cos01_nstrc.csv')
    

    cos,cos_og_no_strc,cos_strc,cos_no_strc = dg.generate_data_sets(input_path='data_set/Cosmetics/sephora.csv',file_name='cos02',
                                                                    output_path='Structuredness/data/Cosmetics/',
                                                                    entity=1000)
    
    print(ssj.profile_table_for_join(cos))
    print(ssj.profile_table_for_join(cos_strc))
    print(ssj.profile_table_for_join(cos_no_strc))
    print(ssj.profile_table_for_join(cos_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    cos = cos.astype('object')
    cos_strc = cos_strc.astype('object')
    cos_no_strc = cos_no_strc.astype('object')
    cos_og_no_strc = cos_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    #unfortunately does not have a ignore column area and one is not able to join with the og_id
    #TODO: is this already blocking??

    print("\n Create structured outputpairs for cos02 \n")
    op_cos_strc = ssj.cosine_join(cos, cos_strc,'og_id','str_id', 'og_Description','str_Description', ws , 0.5,
                                    l_out_attrs=['og_id','og_Description','og_Price','og_Color'],
                                    r_out_attrs=['str_id','str_Description','str_Price','str_Color'],
                                    show_progress=True)

    #labeling
    op_cos_strc = dg.labeling(op_cos_strc,cos_strc,
                                ds_og_id='l_og_id', ds_r_id='r_str_id',
                                strc_id='str_id', strc_og_id='str_og_id')
    
    op_cos_strc.to_csv('Structuredness/data/Cosmetics/outputpairs_cos02_strc.csv')


    #split set into train, validate and test for structure
    train_cos_strc, validate_cos_strc, test_cos_strc = \
                np.split(op_cos_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_cos_strc)), int(.8*len(op_cos_strc))])

    # make them to csv so DeepMatcher can use them
    train_cos_strc.to_csv('Structuredness/data/Cosmetics//train_cos02_strc.csv')
    validate_cos_strc.to_csv('Structuredness/data/Cosmetics/validate_cos02_strc.csv')
    test_cos_strc.to_csv('Structuredness/data/Cosmetics/test_cos02_strc.csv')

    print("\n Create no structured outputpairs cos02 \n")
    op_cos_no_strc = ssj.cosine_join(cos_og_no_strc, cos_no_strc,'og_nstr_id','nstr_id', cos_og_no_strc.columns[-1],cos_no_strc.columns[-1], ws , 0.5,
                                    l_out_attrs=['og_nstr_id',cos_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr_id',cos_no_strc.columns[-1]],
                                    show_progress=True)

    op_cos_no_strc = dg.labeling(op_cos_no_strc,cos_no_strc,
                                ds_og_id='l_og_nstr_id',ds_r_id='r_nstr_id',
                                strc_id='nstr_id', strc_og_id='nstr_og_id') 
    op_cos_no_strc.to_csv('Structuredness/data/Cosmetics/outputpairs_cos02_no_strc.csv')

    #split no struct aws into train, validation and test set
    train_cos_nstrc, validate_cos_nstrc, test_cos_nstrc = \
                np.split(op_cos_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_cos_no_strc)), int(.8*len(op_cos_no_strc))])

    train_cos_nstrc.to_csv('Structuredness/data/Cosmetics/train_cos02_nstrc.csv')
    validate_cos_nstrc.to_csv('Structuredness/data/Cosmetics/validate_cos02_nstrc.csv')
    test_cos_nstrc.to_csv('Structuredness/data/Cosmetics/test_cos02_nstrc.csv')
"""
    #-----------------------------------------------------------------------------
    # 
    #  Data creation for GOOGLE AMAZON PRODUCTS!
    # 
    # ----------------------------------------------------------------------------
"""
    aws,aws_og_no_strc,aws_strc,aws_no_strc = dg.generate_data_sets(input_path='data_set/Amazon-GoogleProducts/Amazon.csv', file_name='aws',
                                                                    output_path='Structuredness/data/Google_AWS/')
    
    print(ssj.profile_table_for_join(aws))
    print(ssj.profile_table_for_join(aws_strc))
    print(ssj.profile_table_for_join(aws_no_strc))
    print(ssj.profile_table_for_join(aws_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    aws = aws.astype('object')
    aws_strc = aws_strc.astype('object')
    aws_no_strc = aws_no_strc.astype('object')
    aws_og_no_strc = aws_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    #unfortunately does not have a ignore column area and one is not able to join with the og_id
    #TODO: is this already blocking??

    print("\n Create structured outputpairs Google AWS\n")
    op_aws_strc = ssj.cosine_join(aws, aws_strc,'og_id','str_id', 'og_name','str_name', ws , 0.5,
                                    l_out_attrs=['og_name','og_description','og_manufacturer','og_price'],
                                    r_out_attrs=['str_name','str_description','str_manufacturer','str_price'],
                                    show_progress=True)
    op_aws_strc = dg.labeling(op_aws_strc,aws_strc,
                                ds_og_id='l_og_id',ds_r_id='r_str_id',
                                strc_id='str_id',strc_og_id='str_og_id')
    
    op_aws_strc.to_csv('Structuredness/data/Google_AWS/outputpairs_aws_strc.csv')


    #split set into train, validate and test for structure
    train_aws_strc, validate_aws_strc, test_aws_strc = \
                np.split(op_aws_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_aws_strc)), int(.8*len(op_aws_strc))])

    # make them to csv so DeepMatcher can use them
    train_aws_strc.to_csv('Structuredness/data/Google_AWS//train_ggl_aws_strc.csv')
    validate_aws_strc.to_csv('Structuredness/data/Google_AWS/validate_ggl_aws_strc.csv')
    test_aws_strc.to_csv('Structuredness/data/Google_AWS/test_ggl_aws_strc.csv')

    print("\n Create no structured outputpairs Google AWS \n")
    op_aws_no_strc = ssj.cosine_join(aws_og_no_strc, aws_no_strc,'og_nstr_id','nstr_id', aws_og_no_strc.columns[-1],aws_no_strc.columns[-1], ws , 0.5,
                                    l_out_attrs=['og_nstr_id',aws_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr_id',aws_no_strc.columns[-1]],
                                    show_progress=True)
    
    op_aws_no_strc = dg.labeling(op_aws_no_strc,aws_no_strc,
                                    ds_og_id='l_og_nstr_id',ds_r_id='r_nstr_id',
                                    strc_id='nstr_id',strc_og_id='nstr_og_id')
    
    op_aws_no_strc.to_csv('Structuredness/data/Google_AWS/outputpairs_ggl_aws_no_strc.csv')

    #split no struct aws into train, validation and test set
    train_aws_nstrc, validate_aws_nstrc, test_aws_nstrc = \
                np.split(op_aws_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_aws_no_strc)), int(.8*len(op_aws_no_strc))])

    train_aws_nstrc.to_csv('Structuredness/data/Google_AWS/train_ggl_aws_nstrc.csv')
    validate_aws_nstrc.to_csv('Structuredness/data/Google_AWS/validate_ggl_aws_nstrc.csv')
    test_aws_nstrc.to_csv('Structuredness/data/Google_AWS/test_ggl_aws_nstrc.csv')

    ggl,ggl_og_no_strc,ggl_strc,ggl_no_strc = dg.generate_data_sets(input_path='data_set/Amazon-GoogleProducts/GoogleProducts.csv',file_name='ggl',
                                                                    output_path='Structuredness/data/Google_AWS/')

    print(ssj.profile_table_for_join(ggl))
    print(ssj.profile_table_for_join(ggl_strc))
    print(ssj.profile_table_for_join(ggl_no_strc))
    print(ssj.profile_table_for_join(ggl_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    ggl = ggl.astype('object')
    ggl_strc = ggl_strc.astype('object')
    ggl_no_strc = ggl_no_strc.astype('object')
    ggl_og_no_strc = ggl_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    print("\n Create structured outputpairs for Google \n")
    op_ggl_strc = ssj.cosine_join(ggl, ggl_strc,'og_id','str_id', 'og_name','str_name', ws , 0.7,
                                    l_out_attrs=['og_name','og_description','og_manufacturer','og_price'],
                                    r_out_attrs=['str_name','str_description','str_manufacturer','str_price'],
                                    show_progress=True)
    
    op_ggl_strc = dg.labeling(op_ggl_strc,ggl_strc,
                                ds_og_id='l_og_id',ds_r_id='r_str_id',
                                strc_id='str_id',strc_og_id='str_og_id')
    
    op_ggl_strc.to_csv('Structuredness/data/Google_AWS/outputpairs_ggl_strc.csv')


    #split set into train, validate and test for structure
    train_ggl_strc, validate_ggl_strc, test_ggl_strc = \
                np.split(op_ggl_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_ggl_strc)), int(.8*len(op_ggl_strc))])

    # make them to csv so DeepMatcher can use them

    train_ggl_strc.to_csv('Structuredness/data/Google_AWS/train_ggl_strc.csv')
    validate_ggl_strc.to_csv('Structuredness/data/Google_AWS/validate_ggl_strc.csv')
    test_ggl_strc.to_csv('Structuredness/data/Google_AWS/test_ggl_strc.csv')


    print("\n Create no structured outputpairs for Google \n")
    op_ggl_no_strc = ssj.cosine_join(ggl_og_no_strc, ggl_no_strc,'og_nstr_id','nstr_id', ggl_og_no_strc.columns[-1],ggl_no_strc.columns[-1], ws , 0.5,
                                    l_out_attrs=['og_nstr_id',ggl_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr_id',ggl_no_strc.columns[-1]],
                                    show_progress=True)
    
    op_ggl_no_strc = dg.labeling(op_ggl_no_strc,ggl_no_strc,
                                    ds_og_id='l_og_nstr_id',ds_r_id='r_nstr_id',
                                    strc_id='nstr_id',strc_og_id='nstr_og_id')
    
    op_ggl_no_strc.to_csv('Structuredness/data/Google_AWS/outputpairs_ggl_no_strc.csv')

    #split no struct ggl into train, validation and test set
    train_ggl_nstrc, validate_ggl_nstrc, test_ggl_nstrc = \
                np.split(op_ggl_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_ggl_no_strc)), int(.8*len(op_ggl_no_strc))])

    train_ggl_nstrc.to_csv('Structuredness/data/Google_AWS/train_ggl_nstrc.csv')
    validate_ggl_nstrc.to_csv('Structuredness/data/Google_AWS/validate_ggl_nstrc.csv')
    test_ggl_nstrc.to_csv('Structuredness/data/Google_AWS/test_ggl_nstrc.csv')
"""
    #-----------------------------------------------------------------------------
    # 
    #  Data creation for Walmart AMAZON!
    # 
    # ----------------------------------------------------------------------------
"""
    aws, aws_og_no_strc, aws_strc, aws_no_strc = dg.generate_data_sets(input_path='data_set/Products_Wallmart_AWS/amazon.csv',file_name='aws',
                                                                        output_path='Structuredness/data/Walmart_AWS/')
    
    print(ssj.profile_table_for_join(aws))
    print(ssj.profile_table_for_join(aws_strc))
    print(ssj.profile_table_for_join(aws_no_strc))
    print(ssj.profile_table_for_join(aws_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    aws = aws.astype('object')
    aws_strc = aws_strc.astype('object')
    aws_no_strc = aws_no_strc.astype('object')
    aws_og_no_strc = aws_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    #unfortunately does not have a ignore column area and one is not able to join with the og_id
    #TODO: is this already blocking??

    print("\n Create structured outputpairs WALMART AWS \n")
    op_aws_strc = ssj.cosine_join(aws, aws_strc,'og_id','str_id', 'og_title','str_title', ws , 0.7,
                                    l_out_attrs=["og_title","og_url","og_asin","og_brand","og_modelno","og_category1","og_pcategory1","og_category2",
                                                "og_pcategory2","og_listprice","og_price","og_prodfeatures","og_techdetails","og_proddescrshort",
                                                "og_proddescrlong","og_dimensions","og_imageurl","og_itemweight","og_shipweight","og_orig_prodfeatures",
                                                "og_orig_techdetails"],
                                    r_out_attrs=["str_title","str_url","str_asin","str_brand","str_modelno","str_category1","str_pcategory1","str_category2",
                                                "str_pcategory2","str_listprice","str_price","str_prodfeatures","str_techdetails","str_proddescrshort",
                                                "str_proddescrlong","str_dimensions","str_imageurl","str_itemweight","str_shipweight","str_orig_prodfeatures",
                                                "str_orig_techdetails"],
                                    show_progress=True)
    
    op_aws_strc = dg.labeling(op_aws_strc,aws_strc,
                                ds_og_id='l_og_id',ds_r_id='r_str_id',
                                strc_id='str_id',strc_og_id='str_og_id')
    
    op_aws_strc.to_csv('Structuredness/data/Walmart_AWS/outputpairs_aws_strc.csv')
     #split set into train, validate and test for structure
    train_aws_strc, validate_aws_strc, test_aws_strc = \
                np.split(op_aws_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_aws_strc)), int(.8*len(op_aws_strc))])

    # make them to csv so DeepMatcher can use them

    train_aws_strc.to_csv('Structuredness/data/Walmart_AWS/train_aws_strc.csv')
    validate_aws_strc.to_csv('Structuredness/data/Walmart_AWS/validate_aws_strc.csv')
    test_aws_strc.to_csv('Structuredness/data/Walmart_AWS/test_aws_strc.csv')


    # no structure run

    #TODO: 
    #need to get for unstructure part the newly created column to join on
    #I believe it is always the last column, but not sure though

    print("\n Create no structured outputpairs WALMART AWS \n")
    op_aws_no_strc = ssj.cosine_join(aws_og_no_strc, aws_no_strc,'og_nstr_id','nstr_id', aws_og_no_strc.columns[-1],aws_no_strc.columns[-1], ws , 0.5,
                                    l_out_attrs=['og_nstr_id',aws_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr_id',aws_no_strc.columns[-1]],
                                    show_progress=True)
    
    op_aws_no_strc = dg.labeling(op_aws_no_strc,aws_no_strc,
                                    ds_og_id='l_og_nstr_id',ds_r_id='r_nstr_id',
                                    strc_id='nstr_id',strc_og_id='nstr_og_id')
    
    op_aws_no_strc.to_csv('Structuredness/data/Walmart_AWS/outputpairs_aws_no_strc.csv')
    #split no struct aws into train, validation and test set
    train_aws_nstrc, validate_aws_nstrc, test_aws_nstrc = \
                np.split(op_aws_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_aws_no_strc)), int(.8*len(op_aws_no_strc))])

    train_aws_nstrc.to_csv('Structuredness/data/Walmart_AWS/train_aws_nstrc.csv')
    validate_aws_nstrc.to_csv('Structuredness/data/Walmart_AWS/validate_aws_nstrc.csv')
    test_aws_nstrc.to_csv('Structuredness/data/Walmart_AWS/test_aws_nstrc.csv')

    wal,wal_og_no_strc,wal_strc, wal_no_strc = dg.generate_data_sets(input_path='data_set/Products_Wallmart_AWS/walmart.csv', file_name='wal',
                                                                        output_path='Structuredness/data/Walmart_AWS/')

    print(ssj.profile_table_for_join(wal))
    print(ssj.profile_table_for_join(wal_strc))
    print(ssj.profile_table_for_join(wal_no_strc))
    print(ssj.profile_table_for_join(wal_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    wal = wal.astype('object')
    wal_strc = wal_strc.astype('object')
    wal_no_strc = wal_no_strc.astype('object')
    wal_og_no_strc = wal_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    #unfortunately does not have a ignore column area and one is not able to join with the og_id
    #TODO: is this already blocking??

    print("\n Create structured outputpairs WALMART\n")
    op_wal_strc = ssj.cosine_join(wal, wal_strc,'og_id','str_id', 'og_title','str_title', ws , 0.5,
                                    l_out_attrs=['og_custom_id','og_id','og_upc','og_brand','og_groupname','og_title','og_price','og_shelfdescr',
                                                'og_shortdescr','og_longdescr','og_imageurl','og_orig_shelfdescr','og_orig_shortdescr','og_orig_longdescr',
                                                'og_modelno','og_shipweight','og_dimensions'],
                                    r_out_attrs=['str_custom_id','str_id','str_upc','str_brand','str_groupname','str_title','str_price','str_shelfdescr','str_shortdescr',
                                                'str_longdescr','str_imageurl','str_orig_shelfdescr','str_orig_shortdescr','str_orig_longdescr','str_modelno',
                                                'str_shipweight','str_dimensions'],
                                    show_progress=True)
    
    op_wal_strc = dg.labeling(op_wal_strc,wal_strc,
                                ds_og_id='l_og_id',ds_r_id='r_str_id',
                                strc_id='str_id',strc_og_id='str_og_id')
    
    op_wal_strc.to_csv('Structuredness/data/Walmart_AWS/outputpairs_wal_strc.csv')


    #split set into train, validate and test for structure
    train_wal_strc, validate_wal_strc, test_wal_strc = \
                np.split(op_wal_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_wal_strc)), int(.8*len(op_wal_strc))])

    # make them to csv so DeepMatcher can use them
    train_wal_strc.to_csv('Structuredness/data/Walmart_AWS/train_wal_strc.csv')
    validate_wal_strc.to_csv('Structuredness/data/Walmart_AWS/validate_wal_strc.csv')
    test_wal_strc.to_csv('Structuredness/data/Walmart_AWS/test_wal_strc.csv')


    # no structure run
    print("\n Create no structured outputpairs WALMART\n")
    op_wal_no_strc = ssj.cosine_join(wal_og_no_strc, wal_no_strc,'og_nstr_id','nstr_id', wal_og_no_strc.columns[-1],wal_no_strc.columns[-1], ws , 0.5,
                                    l_out_attrs=['og_nstr_id',wal_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr_id',wal_no_strc.columns[-1]],
                                    show_progress=True)

    op_wal_no_strc =dg.labeling(op_wal_no_strc,wal_no_strc,
                                ds_og_id='l_og_nstr_id',ds_r_id='r_nstr_id',
                                strc_id='nstr_id',strc_og_id='nstr_og_id')
    
    op_wal_no_strc.to_csv('Structuredness/data/Walmart_AWS/outputpairs_wal_no_strc.csv')

    #split no struct ggl into train, validation and test set
    train_wal_nstrc, validate_wal_nstrc, test_wal_nstrc = \
                np.split(op_wal_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_wal_no_strc)), int(.8*len(op_wal_no_strc))])

    train_wal_nstrc.to_csv('Structuredness/data/Walmart_AWS/train_wal_nstrc.csv')
    validate_wal_nstrc.to_csv('Structuredness/data/Walmart_AWS/validate_wal_nstrc.csv')
    test_wal_nstrc.to_csv('Structuredness/data/Walmart_AWS/test_wal_nstrc.csv')


    print("\n Creation of the data took {} seconds".format(str(time.time()-start)))"""