import pandas as pd
import numpy as np

from data_generator import DataGenerator as dg
from data_generator import ManageStructure as ms
from data_generator import DataProfiling
from data_generator import utilitiy_functions as uf
import data_generator.DataCreator as dc

import py_stringmatching as sm
import py_stringsimjoin as ssj

import time
import random
import sys

from scipy.spatial.distance import cosine
from sklearn.feature_extraction.text import TfidfVectorizer

with open('Structuredness/logs/tst_data_size_stdout.txt', 'w') as sys.stdout:
    
        """
    cos01 =dg.DataGenerator('data_set/Cosmetics/amazon.csv','data_set/Cosmetics/sephora.csv','data_set/Cosmetics/labeled_data.csv',
                            gen_from_cp_reference='rtable_id',bmp_cp_reference='ltable_id',gen_bmp_join='Description')
    cos01.run('cosmetics01','Structuredness/data/test/',3000,cosine_threshold=0.1,nm_factor=4,
                match_multiplier=10,negatives_multiplier=10,numeric=False)

    cos02 =dg.DataGenerator('data_set/Cosmetics/sephora.csv','data_set/Cosmetics/amazon.csv','data_set/Cosmetics/labeled_data.csv',
                            gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='Description')
    cos02.run('cosmetics02','Structuredness/data/test/',3000,cosine_threshold=0.1,nm_factor=4,
                match_multiplier=10,negatives_multiplier=10,numeric=False)

    wal = dg.DataGenerator('data_set/Products_Wallmart_AWS/walmart.csv','data_set/Products_Wallmart_AWS/amazon.csv','data_set/Products_Wallmart_AWS/wal_aws_labeled.csv',
                           gen_from_cp_reference='rtable_id',bmp_cp_reference='ltable_id',gen_bmp_join='title')
    wal.run('wal','Structuredness/data/test/',3000,cosine_threshold=0.1, nm_factor=4,
            match_multiplier=10,negatives_multiplier=10,numeric=False)
    wal_aws = dg.DataGenerator('data_set/Products_Wallmart_AWS/amazon.csv','data_set/Products_Wallmart_AWS/walmart.csv','data_set/Products_Wallmart_AWS/wal_aws_labeled.csv',
                           gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='title')
    wal_aws.run('wal_aws','Structuredness/data/test/',10000,cosine_threshold=0.1,nm_factor=4,
                match_multiplier=3,negatives_multiplier=3,numeric=False) """

        bike01 =dg.DataGenerator('data_set/Bikes/bikedekho.csv','data_set/Bikes/bikewale.csv','data_set/Bikes/labeled_data_copy.csv',
                        gen_from_cp_reference='ltable_id',bmp_cp_reference='rtable_id',gen_bmp_join='bike_name')
        bike01.run('bike01','Structuredness/data/test/',3000,nm_factor=3, nm_severity=0.5,nm_percent=0.3,
                match_multiplier=3,negatives_multiplier=3,numeric=False)
    




