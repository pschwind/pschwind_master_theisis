#!/bin/bash
exec > /ceph/pschwind/datagenerator/Numeric_experiment/logs/ditto_optimized_num_run_location.txt 2>&1
echo ""
echo "Location 01 numeric run"
echo ""
echo ""
echo "Domain knowledge"
echo ""
python train_ditto.py \
  --task experiment/numeric/location01_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --dk product \
  --save_model 
echo ""
echo "Data Augmentation"
echo ""
python train_ditto.py \
  --task experiment/numeric/location01_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --da all \
  --save_model 
echo ""
echo "Summarization"
echo ""
python train_ditto.py \
  --task experiment/numeric/location01_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --summarize \
  --save_model 
echo ""
echo ""
echo "Location 01 no numeric run"
echo ""
echo "Domain knowledge"
echo ""
python train_ditto.py \
  --task experiment/numeric/location01_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --dk product \
  --save_model 
echo ""
echo "Data Augmentation"
echo ""
python train_ditto.py \
  --task experiment/numeric/location01_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --da all \
  --save_model 
echo ""
echo "Summarization"
echo ""
python train_ditto.py \
  --task experiment/numeric/location01_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --summarize \
  --save_model 

echo ""
echo ""
echo ""
echo ""
echo "Location 02 numeric run"
echo ""
echo "Domain knowledge"
echo ""
python train_ditto.py \
  --task experiment/numeric/location02_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --dk product \
  --save_model 
echo ""
echo "Data Augmentation"
echo ""
python train_ditto.py \
  --task experiment/numeric/location02_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --da all \
  --save_model 
echo ""
echo "Summarization"
echo ""
python train_ditto.py \
  --task experiment/numeric/location02_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --summarize \
  --save_model 
echo ""
echo ""
echo "Location 02 no numeric run"
echo ""
echo "Domain knowledge"
echo ""
python train_ditto.py \
  --task experiment/numeric/location02_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --dk product \
  --save_model 
echo ""
echo "Data Augmentation"
echo ""
python train_ditto.py \
  --task experiment/numeric/location02_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --da all \
  --save_model 
echo ""
echo "Summarization"
echo ""
python train_ditto.py \
  --task experiment/numeric/location02_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --summarize \
  --save_model 