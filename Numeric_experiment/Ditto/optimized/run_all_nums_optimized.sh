#!/bin/bash

echo "Start bikes run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/optimized/run_num_bikes_optimized.sh

echo "Start cosmetics run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/optimized/run_num_cosmetics_optimized.sh

echo "Start google run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/optimized/run_num_google_optimized.sh

echo "Start walmart run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/optimized/run_num_walmart_optimized.sh

echo "Start location run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/optimized/run_num_location_optimized.sh