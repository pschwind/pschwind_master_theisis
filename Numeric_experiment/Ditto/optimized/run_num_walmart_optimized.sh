#!/bin/bash
exec > /ceph/pschwind/datagenerator/Numeric_experiment/logs/ditto_optimized_num_run_walmart.txt 2>&1
echo "Numeric"
echo ""
echo "Domain Knowledge"
echo ""
python train_ditto.py \
  --task experiment/numeric/walmart_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --dk product \
  --save_model
echo ""
echo "Data Augmentation"
echo ""
python train_ditto.py \
  --task experiment/numeric/walmart_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --da all \
  --save_model 
echo ""
echo "Summarization"
echo ""
python train_ditto.py \
  --task experiment/numeric/walmart_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --summarize \
  --save_model  
echo ""
echo ""
echo "no num run"
echo ""
echo "Domain Knowledge"
echo ""
python train_ditto.py \
  --task experiment/numeric/walmart_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --dk product \
  --save_model
echo ""
echo "Data Augmentation"
echo ""
python train_ditto.py \
  --task experiment/numeric/walmart_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --da all \
  --save_model 
echo ""
echo "Summarization"
echo ""
python train_ditto.py \
  --task experiment/numeric/walmart_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --summarize \
  --save_model  
