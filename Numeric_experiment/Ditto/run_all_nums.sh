#!/bin/bash

echo "Start bikes run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/run_num_bikes.sh

echo "Start cosmetics run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/run_num_cosmetics.sh

echo "Start google run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/run_num_google.sh

echo "Start walmart run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/run_num_walmart.sh

echo "Start location run"
sh /ceph/pschwind/datagenerator/Numeric_experiment/Ditto/run_num_location.sh