#!/bin/bash
exec > /ceph/pschwind/datagenerator/Numeric_experiment/logs/ditto_num_run_bikes.txt 2>&1

python train_ditto.py \
  --task experiment/numeric/bike_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --save_model 

echo ""
echo "no num run"
echo ""

python train_ditto.py \
  --task experiment/numeric/bike_no_num\
  --batch_size 64 \
  --max_len 128 \
  --lr 3e-5 \
  --n_epochs 50 \
  --finetuning \
  --lm roberta \
  --save_model 