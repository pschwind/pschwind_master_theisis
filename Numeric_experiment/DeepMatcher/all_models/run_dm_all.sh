#!/bin/bash

echo "Start bikes run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/all_models/bikes_numeric_dm.py

echo "Start cosmetics run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/all_models/cosmetics_numeric_dm.py

echo "Start google run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/all_models/google_numeric_dm.py

echo "Start walmart run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/all_models/walmart_numeric_dm.py

echo "Start location run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/all_models/location01_numeric_dm.py
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/all_models/location02_numeric_dm.py