"""
Testing out numeric results for the DeepMatcher
"""

import pandas as pd
import numpy as np
import time 
import sys
import os

import deepmatcher as dm

with open('Numeric_experiment/logs/bikes_all_models_stdout.txt', 'w') as sys.stdout:
       
    #DeepMatcher for structure
    start = time.time()
    if os.path.exists('/ceph/pschwind/datagenerator/Numeric_experiment/data/Bikes/cacheddata.pth'):
        os.remove('/ceph/pschwind/datagenerator/Numeric_experiment/data/Bikes/cacheddata.pth')

    # load data via DeepMatcher
    print("\n Start DeepMatcher with numerics \n")
    train, validation, test = dm.data.process(
        path='Numeric_experiment/data/Bikes/',
        train='bike_train_num.csv', 
        validation='bike_validate_num.csv',
        test='bike_test_num.csv',
        ignore_columns=('index','l_url','l_name','l_owner_type','r_url','r_name','r_owner_type'),
        left_prefix='l_',
        right_prefix='r_',
        label_attr ='gold',
        id_attr='_id')
    
    #rnn best choice from pre runs
    summarizer =['sif', 'rnn', 'attention','hybrid']
    for sum in summarizer:
        model = dm.MatchingModel(attr_summarizer=sum)

            #model.initialize(train)
        model.run_train(
                train,
                validation,
                epochs=25,
                learning_rate=1e-3,
                batch_size=32,
                best_save_path='Numeric_experiment/logs/bikes_all_att_best_save_path.pth')

        pred = model.run_eval(test)

        print("Best F1-score of {} during test was: {}".format(summarizer,str(pred)))

    #delete cachedata.pth so it does not f up the script
    print("\n DeepMatcher ended with numerics \n")
    print("\n Delete chachedata.pth for non-numeric run ")
    if os.path.exists('/ceph/pschwind/datagenerator/Numeric_experiment/data/Bikes/cacheddata.pth'):
        os.remove('/ceph/pschwind/datagenerator/Numeric_experiment/data/Bikes/cacheddata.pth')
          
   # unstructured run
    print("\n Start DeepMatcher w/e numeric \n")
    train_ns, validation_ns, test_ns = dm.data.process(
        path='Numeric_experiment/data/Bikes/',
        train='bike_train_no_num.csv', 
        validation='bike_validate_no_num.csv',
        test='bike_test_no_num.csv',
        ignore_columns=('index','l_id','l_url','l_name','l_owner_type','r_id','r_url','r_name','r_owner_type'),
        left_prefix='l_',
        right_prefix='r_',
        label_attr ='gold',
        id_attr='_id')

    

    summarizer =['sif', 'rnn', 'attention','hybrid']
    for sum in summarizer:
        model = dm.MatchingModel(attr_summarizer=sum)

            #model.initialize(train)
        model.run_train(
                train,
                validation,
                epochs=25,
                learning_rate=1e-3,
                batch_size=32,
                best_save_path='Numeric_experiment/logs/bikes_no_num_best_save_path.pth')

        pred = model.run_eval(test)

        print("Best F1-score of {} during test was: {}".format(summarizer,str(pred)))

    print("DeepMatcher ended for no structure \n") 
    print("DeepMatcher took {} seconds to finish".format(str(time.time()-start)))