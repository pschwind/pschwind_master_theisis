#!/bin/bash

echo "Start bikes run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/bikes_numeric_dm.py

echo "Start cosmetics run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/cosmetics_numeric_dm.py

echo "Start google run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/google_numeric_dm.py

echo "Start walmart run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/walmart_numeric_dm.py

echo "Start location run"
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/location01_numeric_dm.py
python /ceph/pschwind/datagenerator/Numeric_experiment/DeepMatcher/location02_numeric_dm.py