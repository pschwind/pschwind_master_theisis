#!/bin/bash

echo "Start bikes run"
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_bikes_no_num.py
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_bikes_num.py

echo "Start cosmetics run"
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_cosmetic_no_num.py
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_cosmetic_num.py

echo "Start google run"
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_google_no_num.py
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_google_num.py

echo "Start walmart run"
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_walmart_no_num.py
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_walmart_num.py

echo "Start location run"
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_location01_no_num.py
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_location01_num.py

python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_location02_no_num.py
python /ceph/pschwind/datagenerator/Numeric_experiment/Magellan/mag_location02_num.py