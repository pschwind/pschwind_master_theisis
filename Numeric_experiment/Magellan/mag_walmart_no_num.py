"""
Run experiments with the magellan pipeline for structuredness

Learning:
 - need to have same column names, some how preprocessing and storing for Magellan would be dope
"""

import pandas as pd
import numpy as np

import py_entitymatching as em
import py_stringmatching as sm
import py_stringsimjoin as ssj

import sys

#TODO: logging into txt file
with open('Numeric_experiment/logs/mag_walmart_no_num_stdout.txt', 'w') as sys.stdout:

        og = em.read_csv_metadata('Numeric_experiment/data/Walmart_AWS/magellan/walaws_num_og_no_num.csv', key='id')
        strc = em.read_csv_metadata('Numeric_experiment/data/Walmart_AWS/magellan/walaws_gen_no_num.csv',key='id')

        
        # maybe prefix here has to get rid of as well
        op_labeled = em.read_csv_metadata('Numeric_experiment/data/Walmart_AWS/magellan/walaws_num_op_no_num.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')


        train =em.read_csv_metadata('Numeric_experiment/data/Walmart_AWS/walaws_train_no_num.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')
        test =em.read_csv_metadata('Numeric_experiment/data/Walmart_AWS/walaws_test_no_num.csv', key='_id',
                                        ltable=og, rtable=strc,
                                        fk_ltable='l_id',fk_rtable='r_id')

        dt = em.DTMatcher(name='DecisionTree', random_state=0)
        svm = em.SVMMatcher(name='SVM', random_state=0)
        rf = em.RFMatcher(name='RF', random_state=0)
        lg = em.LogRegMatcher(name='LogReg', random_state=0)
        ln = em.LinRegMatcher(name='LinReg')

        feature = em.get_features_for_matching(og,strc,validate_inferred_attr_types=False)

        print(feature.feature_name)

        test_set = em.extract_feature_vecs(train, feature_table=feature, attrs_after='gold',show_progress=False)

        print(test_set[test_set['gold']==1])

        excld_vecs =[col for col in test_set.columns if 'id' in col]
        idx_vecs = [col for col in test_set.columns if 'index' in col]
        url_vecs = [col for col in test_set.columns if 'url' in col]
        asin_vecs = [col for col in test_set.columns if 'asin' in col]
        brand_vecs= [col for col in test_set.columns if 'brand' in col]
        modelno_vecs = [col for col in test_set.columns if 'modelno' in col]
        upc_vecs = [col for col in test_set.columns if 'upc' in col]
        dim_vecs =[col for col in test_set.columns if 'dimensions' in col]
        title_vecs = [col for col in test_set.columns if 'title' in col]
        lngdescr_vecs = [col for col in test_set.columns if 'longdescr' in col]
        grp_vecs = [col for col in test_set.columns if 'groupname' in col]
        

        excld_vecs.extend(idx_vecs)
        #excld_vecs.extend(url_vecs)
        #excld_vecs.extend(asin_vecs)
        #excld_vecs.extend(brand_vecs)
        excld_vecs.extend(modelno_vecs)
        #excld_vecs.extend(dim_vecs)
        excld_vecs.extend(title_vecs)
        excld_vecs.extend(lngdescr_vecs)
        #excld_vecs.extend(grp_vecs)
        excld_vecs.append('gold')

        print(excld_vecs)

        check = test_set.drop(columns=excld_vecs,axis=1)
        print(check.columns.to_list())
        print(check)

        print("Is imputation needed? {}".format(any(pd.notnull(test_set))))

        #SimpleImputer does not accept missing values encoded as NaN natively.
        if any(pd.notnull(test_set)):
                test_set=em.impute_table(test_set, 
                        exclude_attrs=['_id', 'l_id', 'r_id', 'gold'],
                        missing_val= -1,
                        strategy='mean',
                        val_all_nans=0)

        result = em.select_matcher([dt, rf, svm, ln, lg], table=test_set, 
                exclude_attrs=excld_vecs,
                k=5,
                target_attr='gold', metric_to_select_matcher='f1', random_state=0)
        print(result['cv_stats'])
