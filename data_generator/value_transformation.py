#-----------------------------------------------------------------------------------------------
#
# Area to ideate value transformation of copied entities. Idea from ALMSERGen and the referenced
# papers. 
#
#-----------------------------------------------------------------------------------------------

import pandas as pd
import numpy as np
import random
from string import ascii_letters
import copy
from nltk.corpus import product_reviews_1
from pandas.api.types import is_numeric_dtype
from pandas.api.types import is_string_dtype

def transform_values_randomly(ds:pd.DataFrame):

    if not isinstance(ds,pd.DataFrame):
        raise TypeError("ds is not a pd.DataFrame")
    
    severity = get_random_severity()
    percent = get_random_percent()

    numeric_ops = [substract_value, add_value]
    word_ops =[shuffle_word, shuffle_mod_word_level,add_words, delete_words]
    char_ops =[add_random_char, del_random_char,mod_random_char]
    word_char_ops =['word_ops', 'char_ops']

    for cols, values in ds.iteritems():
        if cols != 'id':
            if is_numeric_dtype(ds[cols]): #ds[cols].dtype == int or ds[cols].dtype ==float: #
                num_ops = random.randrange(start=0,stop=len(numeric_ops)-1,step=1)
                for idx, val in values.items():
                    ds.loc[idx, cols]= numeric_ops[num_ops](value_=val,percent=percent)
            else:
                for idx, val in values.items():                                
                    if " " in str(val):            
                        str_ops = random.choice(word_char_ops)
                        if str_ops == 'word_ops':
                            amount_of_ops = random.randrange(start=0,stop=len(word_ops)-1, step=1)
                            for i in range(0,amount_of_ops):
                                if word_ops[i] == shuffle_word:
                                    ds.loc[idx, cols] = word_ops[i](value_=val)
                                else:
                                    ds.loc[idx, cols] = word_ops[i](value_=val, severity=severity)
                        if str_ops == 'char_ops':
                            amount_of_ops = random.randrange(start=0, stop=len(char_ops)-1,step=1)
                            for i in range(0,amount_of_ops):
                                
                                ds.loc[idx, cols] = char_ops[i](value_=val, severity=severity)
                    else:
                        amount_of_ops = random.randrange(start=0, stop=len(char_ops)-1,step=1)
                        for i in range(0,amount_of_ops):
                            
                            ds.loc[idx, cols] = char_ops[i](value_=val, severity=severity)
    return ds

def transform_values(ds:pd.DataFrame, severity:float, percent:float):
    """ This function should allow to transform values by manually selecting severity and percent. Severity handles the changes of strings/objects.
        Percent handle the changes of numeric attributes. This should allow to generate data sets with the capablity to design a gold standard upfront.
        Keep in mind to use these values:
        severity = [0.1,0.2,0.3,0.4,0.5]
        percent = [0.05, 0.1, 0.2]

        Args:
            -ds: DataFrame to change the values
            -severity: Float, which determines the percentage of char, words to be changed
            -percent: Float of how much will be added/substracted from a value percentagewise (e.g.: percent=0.05 => value*0.95 or values*1.05 )
        Returns:
            - ds: DataFrame with transformed values
    
    """
    if not isinstance(ds,pd.DataFrame):
        raise TypeError("ds is not a pd.DataFrame")
    if not isinstance(severity,float):
        raise TypeError("severity is not of type float")
    if not isinstance(percent,float):
        raise TypeError("percent is not of type float")
    
    numeric_ops = [substract_value, add_value]
    word_ops =[shuffle_word, shuffle_mod_word_level,add_words, delete_words]
    char_ops =[add_random_char, del_random_char,mod_random_char]
    word_char_ops =['word_ops', 'char_ops']

    for cols, values in ds.iteritems():
        if cols != 'id':
            if is_numeric_dtype(ds[cols]): #ds[cols].dtype == int or ds[cols].dtype ==float: #
                num_ops = random.randrange(0,len(numeric_ops),1)
                for idx, val in values.items():
                    if val== np.nan or val==0.0 or isinstance(val, bool):
                        break
                    ds.loc[idx, cols]= numeric_ops[num_ops](value_=val,percent=percent)
            else:
                for idx, val in values.items():
                    val = str(val)
                    if len(val)<=1 or val==np.nan:
                        break                                
                    if " " in str(val):            
                        str_ops = random.choice(word_char_ops)
                        if str_ops == 'word_ops':
                            amount_of_ops = random.randrange(start=0,stop=len(word_ops)-1, step=1)
                            for i in range(0,amount_of_ops):
                                if word_ops[i] == shuffle_word:
                                    ds.loc[idx, cols] = word_ops[i](value_=str(val))
                                else:
                                    ds.loc[idx, cols] = word_ops[i](value_=str(val), severity=severity)
                        if str_ops == 'char_ops':
                            amount_of_ops = random.randrange(start=0, stop=len(char_ops)-1,step=1)
                            for i in range(0,amount_of_ops):
                                
                                ds.loc[idx, cols] = char_ops[i](value_=val, severity=severity)
                    else:
                        amount_of_ops = random.randrange(start=0, stop=len(char_ops)-1,step=1)
                        for i in range(0,amount_of_ops):
                            
                            ds.loc[idx, cols] = char_ops[i](value_=val, severity=severity)
    return ds

def transform_numeric_only(ds:pd.DataFrame, percent:float):
    if not isinstance(ds,pd.DataFrame):
        raise TypeError("ds is not a pd.DataFrame")
    if not isinstance(percent,float):
        raise TypeError("percent is not of type float")
    
    numeric_ops = [substract_value, add_value]
    changed_cols=[]
    for cols, values in ds.iteritems():
        if cols != 'id':
            if is_numeric_dtype(ds[cols]): #ds[cols].dtype == int or ds[cols].dtype ==float: #
                changed_cols.append(cols)
                num_ops = random.randrange(0,len(numeric_ops),1)
                for idx, val in values.items():
                    if val== np.nan or val==0.0 or isinstance(val, bool):
                        break
                    ds.loc[idx, cols]= numeric_ops[num_ops](value_=val,percent=percent)
    print("Columns changed: {}".format(str(changed_cols)))
    return ds, changed_cols

def add_random_char(value_, severity):
    """
    copied from ALMSERGen!

    """
    # UnboundLocalError: local variable 'new_value' referenced before assignment
    value_ =str(value_)

    if len(value_)<= 1:
        letts= iter(random.choices(ascii_letters, k=1))
        new_value = value_.join(letts)
    else:
        
        inds = [i for i,_ in enumerate(value_) if not value_.isspace()]

        how_many_additions = int(severity*len(value_))
        how_many_additions = max(1, how_many_additions)

        sam = random.sample(inds, how_many_additions)

        letts =  iter(random.choices(ascii_letters, k=how_many_additions))
        lst = list(value_)
        for ind in sam:
            lst[ind] = next(letts)

        new_value = "".join(lst)
    

    return new_value

def del_random_char(value_, severity):
    """
    copied from ALMSERGen!

    """
    value_length = len(value_)
    
    index_of_chars = [i for i in range(value_length) if value_[i]!=' ']
    how_many_deletions = int(severity*len(index_of_chars))
    how_many_deletions = max(1, how_many_deletions)
    if bool(index_of_chars):
        index_to_remove = np.random.choice(index_of_chars, how_many_deletions, replace=False)
        temp = list(value_)
        for idx in index_to_remove:
            temp[idx] = ''
        new_value = ''.join(temp)
    else:
        temp =''
        new_value =''.join(temp)
  
    return new_value

def mod_random_char(value_, severity):
    """
    copied from ALMSERGen!

    """
    value_length = len(value_)

    if value_length != 0:
    
        index_of_chars = [i for i in range(value_length) if value_[i]!=' ']
        how_many_modifications = int(severity*len(index_of_chars))
        how_many_modifications = max(1, how_many_modifications)
    
        index_to_modify = np.random.choice(index_of_chars, how_many_modifications, replace=False)
        temp = list(value_)
        for idx in index_to_modify:
            temp[idx] = random.sample(ascii_letters, 1)[0]
        new_value = ''.join(temp)
    else:
        new_value = value_

    return new_value

def shuffle_word (value_):
    """
    copied from ALMSERGen!

    """
    words = value_.split()
    random.shuffle(words)
    new_value = ' '.join(words)
    return new_value

def shuffle_mod_word_level(value_, severity):
    """Aligned by ALMSERGen, but simplified that function by excluding BlankChar stuff. Optional to add that as well"""

    new_value = shuffle_word(value_=value_)
    char_mod =['mod', 'del', 'add']
    k = random.randint(1,3)
    operation = random.choices(char_mod, k=k)

    for ops in operation:
        if ops =='mod':
            new_value = mod_random_char(new_value,severity=severity)
        if ops == 'del':
            new_value = del_random_char(new_value,severity=severity)
        if ops == 'add':
            new_value = add_random_char(new_value, severity=severity)
    
    return new_value

def add_words(value_, severity):
    """copied  from ALMSERGen. Added only a nltk corpus to take words from it."""
    try:
        noise_words=product_reviews_1.words('Canon_G3.txt')
        words_count=len(value_.split(' '))

        how_many_noise_words = max(1,int(severity*words_count))
        
        select_random_words = random.choices(noise_words,k=how_many_noise_words)

        new_value = " ".join(select_random_words)
        new_value = new_value + " " + value_
    except: 
        import pdb;pdb.set_trace();
    return new_value

def delete_words(value_, severity):
    """
    copied from ALMSERGen!

    """
    tokens = value_.split(' ')
    remove_tokens =random.sample(list(tokens), int(severity*len(tokens)))
    new_value= copy.copy(value_)
    for rem_token in set(remove_tokens):
        new_value.replace(rem_token, '')
    return new_value

def substract_value(value_, percent):
    """Function to delete percent from a numeric value. Remember 0.05, 0.1, 0.2"""
    return value_*(1-percent)


def add_value(value_, percent):
    """Function to add percent to a numeric value. Remember 0.05, 0.1, 0.2"""
    return value_*(1+percent)

# not sure if needed, because we want to have an overall severity for the data set 
# maybe explicitly choosen

def get_random_severity():
    
    severity = [0.1,0.2,0.3,0.4,0.5]
    return random.choice(severity)

def get_random_percent():
    percent = [0.05, 0.1, 0.2]
    return random.choice(percent)

def deleteCompleteValue(value_):
    """Copied from ALMSERGen. Good for creating missing data!"""
    return None