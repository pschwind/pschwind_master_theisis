import pandas as pd
import numpy as np
import re
import time
import warnings

from pandas.api.types import is_string_dtype
from pandas.api.types import is_numeric_dtype
from pandas.api.types import is_datetime64_dtype

import py_stringsimjoin as ssj
import py_stringmatching as sm

import data_generator.ManageStructure as ms

def get_list_of_unique_values(ds:pd.DataFrame):
    """
    Utility Function to generate a list of [col, [val1, val2,...]] from a DataFrame. Needed for synthetically generate data.

    Arg:
        - ds: DataFrame to get unique values per column from
    Return:
        - unique_col_vals: list of [col, [val1, val2,...]]
    """
    if not isinstance(ds, pd.DataFrame):
        raise TypeError('ds is not of type pd.DataFrame')
    
    unique_col_vals =[]
    
    for column in ds:
        
        unique_col_vals.append(column)
        unique_col_vals.append(ds[column].unique().tolist())

        
    return unique_col_vals

def get_list_of_unique_counts(ds:pd.DataFrame):
    """
    Utility Function to generate a list of [col, int ] from a DataFrame. Needed for synthetically generate data. Especially,
    used for randomly pick one list element from unique_col_vals (get_list_of_unique_values) 

    Arg:
        - ds: DataFrame to get unique values per column from
    Return:
        - count_col_vals: list of [col, int]
    """
    if not isinstance(ds, pd.DataFrame):
        raise TypeError('ds is not of type pd.DataFrame')
    count_col_vals =[]

    for column in ds:
        count_col_vals.append(column)
        count_col_vals.append(ds[column].nunique())
    return count_col_vals


def in_candidate_pairs(ds: pd.DataFrame, cp: pd.DataFrame,ds_id:str, cp_id:str):
    
    ds['in_candpairs']=ds[ds_id].isin(cp[cp_id])
    

    return ds

def standardize_ds_for_creation(og_ds:pd.DataFrame,cp_ds:pd.DataFrame,bmp_ds:pd.DataFrame):

    #standardize prefixes and ids  to enable 
    cp_ds = standardize_cp_prefix(cp=cp_ds)
    cp_ds = standardize_id(ds=cp_ds)

    og_ds = standardize_id(ds=og_ds)
    bmp_ds = standardize_id(ds=bmp_ds)

    og_ds = check_price(og_ds)
    cp_ds = check_price(cp_ds)
    bmp_ds = check_price(bmp_ds)

    return og_ds, cp_ds, bmp_ds

#TODO: Handling of the different IDs and the references in the candidate_pairs.
def standardize_cp_prefix(cp:pd.DataFrame):

    cols = list(cp.columns.values)

    for col in cols:
        if 'l_ltable.' in col:
            old_col = col
            col_name = col.replace('l_ltable.','')
            new_col = 'ltable_'+ col_name
            cp.rename(columns={old_col:new_col},inplace=True)
            continue
        elif 'ltable.' in col:
            old_col = col
            col_name = col.replace('ltable.','')
            new_col = 'ltable_'+ col_name
            cp.rename(columns={old_col:new_col},inplace=True)
            continue
        elif 'l_' in col:
            old_col = col
            col_name = col.replace('l_','')
            new_col = 'ltable_'+ col_name
            cp.rename(columns={old_col:new_col},inplace=True)
            continue
        elif 'r_rtable.' in col:
            old_col = col
            col_name = col.replace('r_rtable.','')
            new_col = 'rtable_'+ col_name
            cp.rename(columns={old_col:new_col},inplace=True)
            continue
        elif  'rtable.' in col:
            old_col = col
            col_name = col.replace('rtable.','')
            new_col = 'rtable_'+ col_name
            cp.rename(columns={old_col:new_col},inplace=True)
            continue
        elif 'r_' in col:  
            old_col = col
            col_name = col.replace('r_','')
            new_col = 'rtable_'+ col_name
            cp.rename(columns={old_col:new_col},inplace=True)
    try:
        cp.columns = cp.columns.str.replace('.','_')
        return cp
    except:
        return cp

def standardize_id(ds: pd.DataFrame, id_name:str ='id'):
    
    id_collection =['ID','custom_id','Product_id']

    for id in id_collection:
        if any(ds.columns.str.contains(id)):
            ds.columns = ds.columns.str.replace(id, id_name)
            #break
    return ds
    

def set_max_length_char(ds:pd.DataFrame, max_length:int):
    """This function will set a max length for every string column and will slice every record after the max_length-th character."""
    cols = ds.columns.to_list()

    for col in cols:
        if is_string_dtype(ds[col]):
            ds[col] = ds[col].str[:max_length]
        elif ds[col].dtypes == 'object':
            ds[col] = ds[col].str[:max_length]
    return ds

def set_max_length_words(ds:pd.DataFrame,max_length:int):
    cols = ds.columns.to_list()

    for col in cols:
        if is_string_dtype(ds[col]):
            ds[col] = ds[col].str.split(n=max_length).str[:max_length].str.join(' ')
        elif ds[col].dtypes == 'object':
            ds[col] = ds[col].str.split(n=max_length).str[:max_length].str.join(' ')

    return ds 

def separate_into_strc_nstrc(ds:pd.DataFrame,nstr_col:str,write:bool=True):
    ds_strc = ds.drop(columns=['l_'+nstr_col,'r_'+nstr_col],axis=1) #,'_sim_score'
    ds_nstrc = ds[['l_id','r_id','l_'+nstr_col,'r_'+nstr_col,'gold']]


    return ds_strc, ds_nstrc

def write_strc_nstrc_to_csv(ds:pd.DataFrame, nstr_col:str, filename:str, output_path:str):
    try:
        if {'in_candpairs', 'og_id'}.issubset(ds.columns):
            strc = ds.drop(columns=[nstr_col,'in_candpairs', 'og_id'],axis=1)
            nstrc = ds[['id', nstr_col]]
        else:
            strc = ds.drop(columns=[nstr_col],axis=1)
            nstrc = ds[['id', nstr_col]]
    except:
        strc = ds.drop(columns=['l_'+nstr_col, 'r_'+nstr_col,'r_in_candpairs', 'r_og_id'],axis=1) #'_sim_score'
        nstrc = ds[['_id','l_id','r_id','l_'+nstr_col, 'r_'+nstr_col,'gold']] 

    strc.to_csv(output_path+filename+'_strc.csv',index=False)
    nstrc.to_csv(output_path+filename+'_nstrc.csv',index=False)

    print("Successfully wrote files {} and {} into {}".format(filename+'_strc.csv',filename+'_nstrc.csv',output_path))

def write_no_num_to_csv(ds:pd.DataFrame, num_col:list, filename:str, output_path:str):
    if {'in_candpairs', 'og_id'}.issubset(ds.columns):
        num = ds.drop(columns=['in_candpairs', 'og_id'],axis=1)
        no_num = num.drop(columns=num_col,axis=1)
    elif {'r_in_candpairs', 'r_og_id'}.issubset(ds.columns):
        num = ds.drop(columns=['r_in_candpairs', 'r_og_id'],axis=1)
        no_num = num.drop(columns=num_col,axis=1)
    else:
        try:
            num = ds.copy()
            no_num = ds.drop(columns=num_col,axis=1)
        except:
            num_col.remove('in_candpairs')
            num = ds.copy()
            no_num = ds.drop(columns=num_col,axis=1)

    no_num.to_csv(output_path+filename+'_no_num.csv',index=False)
    num.to_csv(output_path+filename+'_num.csv',index=False)

def exclude_similiar_entities(target:pd.DataFrame, source:pd.DataFrame,target_id:str,source_id:str,join_col:str):
    
    t = target
    s = source
    t = pd.concat([t,s],axis=0).drop_duplicates(keep=False)
    #ent_id = target[~target[target_id].isin(source[source_id])]
    if not t[target_id].is_unique:
        t[target_id] = np.arange(len(t))

    ws = sm.WhitespaceTokenizer()
    ent_ssj = ssj.cosine_join(t, s,target_id,source_id,join_col,join_col,ws,1,l_out_attrs=[target_id],r_out_attrs=[source_id])
    ent_ssj = t[~t[target_id].isin(ent_ssj['l_'+target_id])]

    #non_sim_ent = pd.merge(ent_ssj,ent_id,how='inner',on=target_id)
    if not ent_ssj.empty:
        #non_sim_ent = ent_ssj.merge(ent_id,how='inner')
        return ent_ssj
    else:
        print(warnings.warn("No similar entities discovered!!", UserWarning))
    
def check_similiar_entities(target:pd.DataFrame, source:pd.DataFrame):
    try:
        comparison = target.merge(source, indicator=True, how='outer')
        comparison = comparison[comparison['_merge']=='both']
    except:
        target = target.astype('object')
        source = source.astype('object')
        comparison = target.merge(source, indicator=True, how='outer')
        comparison = comparison[comparison['_merge']=='both']

    if comparison.empty:
        return True
    else:
        return False

def optimize_memory_usage(ds:pd.DataFrame, int:int, float:int):
    ds = ms.infere_dtypes(ds=ds)

    lst_col = ds.columns.to_list()

    for col in lst_col:
        if is_numeric_dtype(ds[col]):
            try:
                ds[col]=ds[col].astype('float'+str(float))
                
            except:
                try:
                    ds[col]=ds[col].astype('int' +str(int))
                except:
                    ds[col]=ds[col].astype('bool')

    return ds

def check_price(ds:pd.DataFrame):

    price_names = ['price','Price','prices','Prices']

    for prc in price_names:
        if prc in ds.columns:
            if is_string_dtype(ds[prc]):
                ds[prc] = ds[prc].str.replace('$', '')
                ds[prc] = ds[prc].str.replace('€', '')
                try:    
                    ds[prc] = ds[prc].astype('float')
                except:
                    ds[prc] =ds[prc].astype('string') 

    return ds