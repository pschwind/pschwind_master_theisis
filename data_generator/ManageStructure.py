from typing import Type
import pandas as pd

from pandas.api.types import is_numeric_dtype
from pandas.api.types import is_string_dtype
from pandas.api.types import is_datetime64_any_dtype

import random
"""
Idea of this class/script is to enable structural changes of a given data set. What is needed for that is information about 
data qaulity and schema information (as good as possible). Once we have an overview about the already existing schema, we are
able to create a structure score (0-1). The data quality information is probably needed for contextual changes(?).
"""

def transfromDataType (ds: pd.DataFrame, structure: bool =True) -> pd.DataFrame:
    """
    transform data set into obj dtype or into the ones they want via a structure boolean

    :param ds: DataFrame to change
    :param structure: Bool to indicate what DType structure you want
    :return: DataFrame of DataTypes that hint structure or non-structure
    """
    # raise error in case the parameters have the wrong type
    if not isinstance(ds, pd.DataFrame):
        raise TypeError
    elif not isinstance(structure, bool):
        raise TypeError
    
    if structure:
        #assign as good as possible all the right data types to data frame
        ds = ds.convert_dtypes()
    else:
        # assign all columns the datatype obj
        ds = ds.astype(object)


    return ds

def get_DataTypes(ds:pd.DataFrame):
    """Get list of Data types in order of appearance into a list. Return that list!"""

    lst_dtypes = ds.dtypes.to_list()
    print(len(lst_dtypes))
    for dtype in lst_dtypes:
        print(dtype)
        idx = lst_dtypes.index(dtype)
        if dtype in ["string[python]","string"]:
            lst_dtypes[idx] = 'string'
        elif dtype in ["dtype('float64')","dtype('float32')","float", "float32","float64"]:
            lst_dtypes[idx] = 'float'
        elif dtype in ["dtype('int32')","dtype('int64')","int", "int32", "int64"]:
            lst_dtypes[idx] = 'int'
        elif dtype in ["dtype('datatime64')","dtype('datatime64[ns]')","date","datetime64","datetime64[ns]"]:
            lst_dtypes[idx] = 'date'
        elif  dtype in ["dtype('bool')", "bool[python]","bool"]:
            lst_dtypes[idx] = 'bool'
        else:            
            lst_dtypes[idx] = 'object'
    return lst_dtypes


def infere_dtypes(ds:pd.DataFrame):
    """
    This function inferes from a DataFrame the Dtypes in a very simple way. It will distinguish between float, string and objects.
    It will return a a new dataframe with infered Dtypes for structure. 
    """
    for col in ds:
        if is_numeric_dtype(ds[col]):
            try:
                ds[col]=ds[col].astype('int')
            except:
                try:
                    ds[col]=ds[col].astype('float')
                except:
                    ds[col]=ds[col].astype('bool')
        elif is_datetime64_any_dtype(ds[col]):
            ds[col]=ds[col].astype('datetime64[ns]')

        #everything else is a string in pandas it is of type object
        else:
            ds[col]=ds[col].astype('object')
    
    return ds

#TODO: add max_length string and some random shuffling of the cols to combine (random sorting)
def denormalize_DF(df:pd.DataFrame, cols_to_combine:list, max_length: int =1, shuffle:bool =False ):
    """
    This function will take a dataframe and will denormalize aka unstructure the dataframe based on the passed columns and add
    the unstructured dataframe as additional column to the ds.

    Args:
        -df: DataFrame to unstrucuture
        -cols_to_combine: columns to combine to a larger string (= unstructuring)
        - max_length: int value that sets limits of words for the denormalized column (Ideally: max_length >1).
    Result:
        -df: DataFrame with added unstructured column
    """

    if not isinstance(df, pd.DataFrame):
        raise TypeError('df is not a DataFrame')
    if not isinstance(cols_to_combine, list):
        raise TypeError('cols_to_combine is not a list')
    if not isinstance(max_length,int):
        raise TypeError('max_length is not a integer')
    if not isinstance(shuffle,bool):
        raise TypeError('shuffle needs to be a bool (True/False)')
    if shuffle:
        random.shuffle(cols_to_combine)

    new_col_name =""
    for col in cols_to_combine:
        new_col_name += '_' + str(col)

    df[new_col_name] = " "
    df[new_col_name] = df[new_col_name].fillna("_")
    df[new_col_name] = df[new_col_name].astype('string')

    
    for idx, row in df.iterrows():
        comb_cols =""
        for col in cols_to_combine:
            comb_cols += str(row[col]) +" "

        df.at[idx,new_col_name] = str(comb_cols.encode('utf-8'))


    if max_length>1:
        df[new_col_name] = df[new_col_name].str.split(n=max_length).str[:max_length].str.join(' ')
  
    return df, new_col_name

def denormalize(df:pd.DataFrame, excluded_cols:list,max_length:int =1):
    cols = df.columns.to_list()

    for ele in excluded_cols:
        cols.remove(ele)
    
    random.shuffle(cols)
    df = denormalize_DF(df,cols_to_combine=cols, max_length=max_length)
    return df