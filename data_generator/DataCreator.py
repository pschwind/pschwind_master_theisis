import pandas as pd
import numpy as np

from data_generator import DataGenerator as dg
from data_generator import ManageStructure as ms
from data_generator import DataProfiling
from data_generator import utilitiy_functions as uf

import py_stringmatching as sm
import py_stringsimjoin as ssj

import time
import random
import sys

from scipy.spatial.distance import cosine
from sklearn.feature_extraction.text import TfidfVectorizer

class DataCreator:
    def __init__(self, gen_from:str,bmp:str, bm_cp:str,cp_ltable:str,gen_bmp_join:str,filename:str,output_path:str):
        self.gen_from = gen_from
        self.bmp =bmp
        self.bm_cp = bm_cp
        self.filename =filename
        self.output_path =output_path
        self.gen_bmp_join = gen_bmp_join

        if cp_ltable == 'og':
            self.og_cp_reference = 'ltable_id'
            self.bmp_cp_reference='rtable_id'
        elif cp_ltable =='gen':
            self.og_cp_reference = 'rtable_id'
            self.bmp_cp_reference='ltable_id'
        else:
            raise ValueError("cp_ltable must be either \'og\' or \'gen\'.")
    
    def run(self,gen_size:int,cosine_threshold:float,non_match_factor:int=3):

        og_name = self.filename + '_og'
        gen_name = self.filename +'_gen'
        op_name = self.filename +'_op'

        og,test = self.create_data(gen_from=self.gen_from, benchmark_partner=self.bmp, benchmark_candidate_pair= self.bm_cp,
                            gen_from_cp_reference=self.og_cp_reference,bmp_cp_reference=self.bmp_cp_reference,gen_bmp_join=self.gen_bmp_join,
                            gen_size=gen_size,non_match_factor=non_match_factor)
        
       

        og.to_csv(self.output_path + og_name +'.csv')
        test.to_csv(self.output_path + gen_name +'.csv')

        cols = og.columns.to_list()
        cols.remove('id')
        random.shuffle(cols)

        og, combined_col = ms.denormalize_DF(og,cols_to_combine=cols,max_length=200)
        test, combined_col = ms.denormalize_DF(test,cols_to_combine=cols,max_length=200)

        uf.write_strc_nstrc_to_csv(og,combined_col,og_name,self.output_path)
        uf.write_strc_nstrc_to_csv(test,combined_col,gen_name,self.output_path)

        og = og.astype('object')
        test = test.astype('object')

        print("\nProfiles for joining: 1. og 2. gen")
        print(ssj.profile_table_for_join(og))
        print(ssj.profile_table_for_join(test))

        ws = sm.WhitespaceTokenizer()
        output_pair = ssj.cosine_join(og,test, 'id','id',og.columns[-1],test.columns[-1],ws,threshold=cosine_threshold,
                                    l_out_attrs=og.columns.to_list(),
                                    r_out_attrs=test.columns.to_list())

        print(output_pair[output_pair['_sim_score']==1])

        output_pair = dg.labeling(output_pair, test,ds_og_id='l_id',ds_r_id='r_id',gen_id='id',gen_og_id='og_id')

        uf.write_strc_nstrc_to_csv(output_pair,combined_col,op_name,self.output_path)

        print(output_pair['gold'].value_counts())

        train, validate, tst = dg.train_validate_test_split(output_pair,label='gold',file_name=self.filename,output_path=self.output_path)

        print("\nMatch-ratio")
        print("train: {}".format(str(DataProfiling.get_match_ratio(train,'gold'))))
        print("validate: {}".format(str(DataProfiling.get_match_ratio(validate,'gold'))))
        print("test: {}".format(str(DataProfiling.get_match_ratio(tst,'gold'))))

        train_strc,train_nstrc = uf.separate_into_strc_nstrc(train,nstr_col=combined_col)
        validate_strc, validate_nstrc = uf.separate_into_strc_nstrc(validate, nstr_col=combined_col)
        test_strc,test_nstrc =uf.separate_into_strc_nstrc(tst,nstr_col=combined_col)

        train_strc.to_csv(self.output_path + self.filename +'_train_strc.csv',index=False)
        train_nstrc.to_csv(self.output_path + self.filename +'_train_nstrc.csv',index=False)

        validate_strc.to_csv(self.output_path + self.filename +'_validate_strc.csv',index=False)
        validate_nstrc.to_csv(self.output_path + self.filename +'_validate_nstrc.csv',index=False)

        test_strc.to_csv(self.output_path + self.filename +'_test_strc.csv',index=False)
        test_nstrc.to_csv(self.output_path + self.filename +'_test_nstrc.csv',index=False)


