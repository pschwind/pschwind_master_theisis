from data_generator import DataGenerator as dg

example = dg.DataGenerator(gen_from='DataSetA',benchmark_partner='DataSetB',benchmark_candidate_pair='LabeledDataSetAB',
                        gen_from_cp_reference='A_ID',bmp_cp_reference='B_ID',gen_bmp_join='title')
example.run(filename='example',output_path='dummy/output/path/', entities=1000,nm_factor=3,nm_severity=0.3,nm_percent=0.1,
            match_multiplier=4, negatives_mulitplier=3,numeric=False)