"""
This file is used to test out the experiment setup of this thesis.

Expectations:
- structure manipulation of data sets works
- manipulation of numeric attribute decisiveness works
- entity matching with dataset does make sense

Test scenarios:
- Entity Matching with a high structured data sets
- Entity Matching with no structured data sets
- Entity Matching with numeric attribute decisiveness
- Entity Matching with string attribute dominant matching

Current focus on DeepMatcher

"""

#----------------------------------------------------------------------------------------
#
#
# General EM-process:
# 1. load and generate data
# 2. preprocess data
# 3. Blocking 
# 4. Create candidate pairs
# 5. Create goldstandard
# 5. Create train, val, test split
# 6. Learn and apply model
# 7. save and log results of model
#
#
#---------------------------------------------------------------------------------------

import pandas as pd
import numpy as np
import sys

import py_entitymatching as em
import py_stringmatching as sm
import py_stringsimjoin as ssj
import deepmatcher as dm

from data_generator import DataGenerator as dg
from data_generator import ManageStructure as ms


with open('DeepLearning\logs\\em_test_nstrc_stdout.txt', 'w') as sys.stdout:



    # load data via DeepMatcher

    train, validation, test = dm.data.process(
        path='data_set\Amazon-GoogleProducts\DeepMatcher\EM_test',
        train='train_aws_nstrc.csv', 
        validation='validate_aws_nstrc.csv',
        test='test_aws_nstrc.csv',
        left_prefix='l_og_nstr.',
        right_prefix='r_nstr.',
        label_attr ='gold',
        id_attr='_id')
    
    summarizer =['sif', 'rnn', 'attention','hybrid']
    for sum in summarizer:

        model = dm.MatchingModel(attr_summarizer=sum)

        #model.initialize(train)
        model.run_train(
            train,
            validation,
            epochs=25,
            learning_rate=1e-3,
            batch_size=32,
            best_save_path='data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/best_save_path_nstrc.pth')

        pred = model.run_eval(test)

        print("Best F1-score of {} during test was: {}".format(sum, pred))



