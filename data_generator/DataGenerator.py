"""
More or less the basic class that will consolidate other parts for the data generation
"""


import data_generator.DataProfiling as dp
import data_generator.ManageStructure as ms
import pandas as pd
import numpy as np
import random
from pandas.api.types import is_string_dtype

import data_generator.utilitiy_functions as uf 
from data_generator.value_transformation import transform_values_randomly
from data_generator.value_transformation import transform_values
from data_generator.value_transformation import transform_numeric_only

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder 

import py_stringsimjoin as ssj
import py_stringmatching as sm
import scipy
import warnings
import math

class DataGenerator:
    def __init__(self,gen_from:str,benchmark_partner:str, benchmark_candidate_pair:str,gen_from_cp_reference:str, bmp_cp_reference:str,
                gen_bmp_join:str):
        self.gen_from = gen_from
        self.benchmark_partner = benchmark_partner
        self.benchmark_candidate_pair = benchmark_candidate_pair
        self.gen_from_cp_reference = gen_from_cp_reference
        self.bmp_cp_reference = bmp_cp_reference
        self.gen_bmp_join = gen_bmp_join

    def check_matches_in_negatives(self):
        
        bmp = self.bmp_negatives
        cp = self.cp_negatives
        rnd_neg = self.neg_fill
        m = self.matches
        if not bmp['id'].is_unique:
            bmp['id'] = np.arange(len(bmp))
        if not cp['id'].is_unique:
            cp['id'] = np.arange(len(cp))
        if not rnd_neg['id'].is_unique:
            rnd_neg['id'] = np.arange(len(rnd_neg))
        if not m['id'].is_unique:
            m['id'] = np.arange(len(m))
        
        ws =sm.WhitespaceTokenizer()

        bmp_check = ssj.cosine_join(bmp,m,'id','id',self.gen_bmp_join,self.gen_bmp_join,ws,1,l_out_attrs=['id'],r_out_attrs=['id'])
        cp_check = ssj.cosine_join(cp,m,'id','id',self.gen_bmp_join,self.gen_bmp_join,ws,1,l_out_attrs=['id'],r_out_attrs=['id'])
        rnd_neg_check = ssj.cosine_join(rnd_neg,m,'id','id',self.gen_bmp_join,self.gen_bmp_join,ws,1,l_out_attrs=['id'],r_out_attrs=['id'])

        if not bmp_check.empty:
            print("\n BMP:\n")
            print(bmp_check)
            
        if not cp_check.empty:
            print("\n CP:\n")
            print(cp_check)
        if not rnd_neg_check.empty:
            print("\n RNG:\n")
            print(rnd_neg_check)

        
    
    def labeling(self, ds_og_id:str, ds_r_id:str, gen_id:str, gen_og_id:str):
        """
        This function should take a candidate pair and label to identify matches (1) and non-matches (0). It also keeps the
        similiarity score now, to enable better creation of matches and non-matches.
        Args:
            - ds_og_id: column name as string of the left id of the candidate pairs (in this scenario usually called og)
            - ds_r_id: column name as string of the right id of the candidate pairs
            - gen_id: column name as string of the generated id of the generated Dataframe
            - gen_og_id: column name as string of the original id (before transformations) of the generated Dataframe
        Returns:
            - output_pairs: Dataframe of candidate pairs with an added column for labels
        """ 
             
        try:
            jh_op = self.output_pair[[ds_og_id,ds_r_id]]
            jh_gen =self.gen_ds[[gen_id,gen_og_id]]

            jh_op = jh_op.rename(columns={ds_r_id:gen_id})

            helper_join=pd.merge(jh_op,jh_gen,how='left')
            label = np.where(helper_join[ds_og_id]==helper_join[gen_og_id],1,0)
            
            try:
                self.output_pair['gold'] = label
            except:
                self.output_pair['gold'] = pd.Series(label)
                self.output_pair['gold'] = self.output_pair['gold'].fillna(value= -1)
            
            print("Information about SIM SCORE:\n")
            print(dp.sim_score_deviation(ds=self.output_pair,col_name='_sim_score'))

            #ds = ds.drop(columns=['_sim_score'],axis=1)
                
            self.output_pair['gold'] = self.output_pair['gold'].astype('int')
            #ds = ds.set_index('_id')
        except:
            jh_op = self.output_pair[[ds_og_id,ds_r_id]].astype('object')
            jh_gen =self.gen_ds[[gen_id,gen_og_id]].astype('object')

            jh_op = jh_op.rename(columns={ds_r_id:gen_id})

            helper_join=pd.merge(jh_op,jh_gen,how='left')
            label = np.where(helper_join[ds_og_id]==helper_join[gen_og_id],1,0)
            try:
                self.output_pair['gold'] = label
            except:
                self.output_pair['gold'] = pd.Series(label)
                self.output_pair['gold'] = self.output_pair['gold'].fillna(-1)

            print("Information about SIM SCORE:\n")
            print(dp.sim_score_deviation(ds=self.output_pair,col_name='_sim_score'))
            #ds = ds.drop(columns=['_sim_score'],axis=1)
                
            self.output_pair['gold'] = self.output_pair['gold'].astype('int')
            #ds = ds.set_index('_id') 
                

        #in case sth gets wrongly assigned (check if that would be sufficient)
        condition_wrong_assign =(self.output_pair[ds_og_id]==self.output_pair['r_'+gen_og_id]) & (self.output_pair['gold']==0)
        self.output_pair.loc[condition_wrong_assign,'gold']=1
        #in case some got overlooked and also an indication if negatives are true negatives
        condition_sim_score = (self.output_pair['_sim_score']==1) & (self.output_pair['gold']==0)
        self.output_pair.loc[condition_sim_score,'gold']=1

        return self.output_pair

    def train_validate_test_split(self,label:str,file_name:str,output_path:str):
        """
        The goal is to split a DataFrame into stratified portions of train, validate, and test. The ratio should be 60,20,20.
        Returned should be 3 data sets for the dataframe at hand.

        """
        
        train_val, test = train_test_split(self.output_pair,test_size=0.2,stratify=self.output_pair[label])
        train, validate = train_test_split(train_val,test_size=0.25,stratify=train_val[label])

        # transform list into DataFrame
        self.train = pd.DataFrame(train, columns=self.output_pair.columns)
        self.validate = pd.DataFrame(validate,columns=self.output_pair.columns)
        self.test = pd.DataFrame(test,columns=self.output_pair.columns) 

        train.to_csv(output_path +'train_'+ file_name +'.csv',index=False)
        validate.to_csv(output_path +'validate_'+file_name+'.csv',index=False)
        test.to_csv(output_path +'test_'+file_name+'.csv',index=False)

        return train, validate, test

    def create_data(self,gen_size:int,non_match_factor:int=3,nm_severity:float=0.3,nm_percent:float=0.1):
        """
        Function to better generate and also control the to be generated data sets better. Idea is to use a subset of the gen_from data set
        and generate data based on a ratio of 1 match to non_match_factor (3) non-matches. Non-matches get created by using the benchmark_partner
        and the benchmark candidate pairs(sample + benchmark_parter => label 0).
        Args:
            - gen_from: string to the path of the data set to generate from
            - benchmark_partner: string to the benchmark partner (e.g from WDC, Magellan, Uni-Leipzig)
            - benchmark_candidate_pair: string to the already existing candidate pair of gen_from and benchmark_partner
            - gen_size: integer to specify how many records/entities the generated data set should have
            - non_match_factor: Integer to specify how many non-matches should be generated for 1 match (default: 3)
        Return:
            - gen_ds: DataFrame of the created data    
        """

        #TODO: Create part for wrong entries to catch them and Raise Error messages
        # load the data sets:
        self.gen_size = gen_size
        self.non_match_factor = non_match_factor
        randomly_generated =0
        

        try:
            og_ds = pd.read_csv(self.gen_from,encoding='unicode_escape')
            bp_ds = pd.read_csv(self.benchmark_partner, encoding='unicode_escape')
        except:
            og_ds = pd.read_csv(self.gen_from)
            bp_ds = pd.read_csv(self.benchmark_partner)
        try:
            cp_ds = pd.read_csv(self.benchmark_candidate_pair)
        except:
            cp_ds = pd.read_csv(self.benchmark_candidate_pair, encoding='unicode_escape')

        #standardize the prefix of cp and the ids of the datasets
        og_ds,cp_ds,bp_ds = uf.standardize_ds_for_creation(og_ds=og_ds,cp_ds=cp_ds,bmp_ds=bp_ds)

        #get number of matches needed
        sampled_og_ds = og_ds.sample(n=gen_size) #,replace=True

        match_size = round(gen_size/(non_match_factor +1))

        if match_size > len(og_ds.index):
            max_size = len(og_ds.index)*(non_match_factor+1)
            raise ValueError("The size of the to be generated data frame is too big. Max size is {}".format(str(max_size)))
        
        # sample the needed match size from og_ds
        self.matches = sampled_og_ds.sample(match_size)
        #og_rest = pd.concat([sampled_og_ds, self.matches],axis=0).drop_duplicates(keep= False)
        og_rest = uf.exclude_similiar_entities(og_ds,sampled_og_ds,'id','id',self.gen_bmp_join)

        assert not any(og_rest['id'].isin(self.matches['id'])), "some matches are still in og_rest ds !! check drop_duplicates!"
        assert not any(og_rest['id'].isin(sampled_og_ds['id'])), "some samples are still in og_rest ds !! check drop_duplicates!"

        negative_size = round((gen_size - match_size))
        hard_negatives_ratio = 0.8
        hard_negatives_size = round(negative_size*hard_negatives_ratio)
        rest_negative_size = negative_size-hard_negatives_size

        #non matches from candidate pairs as one part
        m_in_cp = uf.in_candidate_pairs(self.matches,cp_ds,'id',self.gen_from_cp_reference)
        m_in_cp = m_in_cp[m_in_cp['in_candpairs']==True]

        assert not m_in_cp.empty, "NO matches in candidate pair!!"
        
        if len(m_in_cp.index)>0:
            
            cp_filtered = cp_ds.merge(m_in_cp,how='inner',left_on=self.gen_from_cp_reference,right_on='id')
            cp_filtered = cp_filtered[cp_filtered['gold']==0]
            cp_filtered = cp_filtered[cp_ds.columns.to_list()]

            self.cp_negatives = bp_ds.merge(cp_filtered,how='inner', left_on='id',right_on=self.bmp_cp_reference)
            self.cp_negatives = self.cp_negatives[bp_ds.columns.to_list()]
            self.cp_negatives = uf.exclude_similiar_entities(self.cp_negatives,sampled_og_ds,'id','id',self.gen_bmp_join)

        else:
            randomly_generated += rest_negative_size
            self.cp_negatives = og_rest.sample(n=rest_negative_size)
        
        if len(self.cp_negatives.index)< rest_negative_size:
            warnings.warn("The negatives taken from the candidate pair will be added with samples from the benchmark partner",UserWarning)
            diff = rest_negative_size - len(self.cp_negatives.index)
            diff_ds = og_rest.sample(n=diff,replace=True)
            #get assertion error solved by changing values drastical and reset id
            diff_ds = transform_values(diff_ds,severity=0.5,percent=0.2)
            

            # Append seems not to work !!
            self.cp_negatives = pd.concat([self.cp_negatives,diff_ds],axis=0, ignore_index=True)
            randomly_generated += diff

        elif len(self.cp_negatives.index)> rest_negative_size:
            warnings.warn("The negatives taken from the candidate pair exceed their percentage of the negative matching entities. Some samples will be cut",UserWarning)
            surplus = len(self.cp_negatives.index)-rest_negative_size
            self.cp_negatives = self.cp_negatives.iloc[:-surplus] 
        
        assert uf.check_similiar_entities(self.cp_negatives,sampled_og_ds), "CP_Negative entity is in sampled og ds."
        assert uf.check_similiar_entities(self.cp_negatives,self.matches), "CP_Negative entity is in matches."

        #non matches from benchmark partner (high similiarity)
        
        bp_filtered = uf.exclude_similiar_entities(bp_ds,self.matches,'id','id',self.gen_bmp_join)
        bp_filtered = uf.exclude_similiar_entities(bp_filtered,sampled_og_ds,'id','id',self.gen_bmp_join)

        assert not bp_filtered.empty, "bp_filtered seems to be empty, check uf.exclude_similiar_entities()"
        ws =sm.WhitespaceTokenizer()
        op_bp_m = ssj.cosine_join(bp_filtered,self.matches,'id','id',self.gen_bmp_join,self.gen_bmp_join,ws,0.1, l_out_prefix='bp_',r_out_prefix='m_')
        op_bp_m = op_bp_m.sort_values('_sim_score',ascending=False)
        

        if op_bp_m.empty:
            raise ValueError("No similiarity between filtered benchmark partner and matches detected! That is strange! Very strange")
        elif len(op_bp_m.index)< hard_negatives_size:
            diff = hard_negatives_size - len(op_bp_m.index)
            self.bmp_negatives = bp_ds[bp_ds['id'].isin(op_bp_m['bp_id'])]
            diff_ds = bp_filtered.sample(diff,replace=True)#bp_filtered.index.values,diff,replace=False)
            #diff_ds = bp_filtered.iloc[diff_ds]
            self.bmp_negatives = pd.concat([self.bmp_negatives,diff_ds],axis=0,ignore_index=True)
            randomly_generated += diff
        elif len(op_bp_m.index)> hard_negatives_size:
            #surplus = len(op_bp_m.index)-negative_size
            op_bp_m = op_bp_m.iloc[0:(hard_negatives_size-1)]
            self.bmp_negatives = bp_ds[bp_ds['id'].isin(op_bp_m['bp_id'])]  
        else:
            self.bmp_negatives = pd.merge(bp_filtered,op_bp_m,how='inner', left_on='id', right_on='bp_id')
            self.bmp_negatives = self.bmp_negatives[bp_ds.columns.to_list()]
            self.bmp_negatives = self.bmp_negatives[:(hard_negatives_size-1)]

        assert uf.check_similiar_entities(self.bmp_negatives,sampled_og_ds), "bmp_negatives entity is in sampled og ds."
        assert uf.check_similiar_entities(self.bmp_negatives,self.matches), "bmp_negatives entity is in matches."           

        # rest random sampling from benchmark partner and/or others (??)
        negative_diff = gen_size - match_size - len(self.cp_negatives.index) - len(self.bmp_negatives.index)
        randomly_generated += negative_diff
        self.neg_fill = pd.DataFrame()
        if negative_diff < 0:
            raise ValueError("The amount of entries exceeded the defined size!")
        if negative_diff>0:
            # not only check for ID but also similiarity to matches (to be sure!) - Cant I use bp_filtered??
            bp_check = uf.exclude_similiar_entities(bp_ds,self.matches,'id','id',self.gen_bmp_join)
            bp_check = uf.exclude_similiar_entities(bp_check,sampled_og_ds,'id','id',self.gen_bmp_join)
            bp_check = bp_check[~bp_check['id'].isin(self.bmp_negatives['id'])]
            bp_check = bp_check[~bp_check['id'].isin(self.cp_negatives['id'])]
            
            if bp_check.empty:
                bmp_n_copy = transform_values(self.bmp_negatives,severity=nm_severity, percent=nm_percent)
                cp_n_copy = transform_values(self.cp_negatives,severity=nm_severity,percent=nm_percent)
                bmp_part = bmp_n_copy.sample(n=round(negative_diff/2),replace=True)
                cp_part = cp_n_copy.sample(n=round(negative_diff/2),replace=True)
                self.neg_fill = pd.concat([bmp_part,cp_part],axis=0)
            elif len(bp_check.index) < negative_diff:
                #self.neg_fill = np.random.choice(bp_check.index.values, negative_diff,replace=True)
                self.neg_fill = bp_check.sample(negative_diff,replace=True)
            elif len(bp_check.index) > negative_diff:
                surplus = len(bp_check.index) - negative_diff 
                self.neg_fill = bp_check.iloc[:-surplus]


        # save og_id for labeling purposes later
        og_id = self.matches['id'].to_list()

        #og_id.extend(self.cp_negatives['id'].to_list())
        #og_id.extend(self.bmp_negatives['id'].to_list())
        if self.numeric:
            self.matches, self.m_num_col = transform_numeric_only(self.matches,percent=0.1)
            self.matches['og_id']= og_id
            self.cp_negatives, self.cp_num_col = transform_numeric_only(self.cp_negatives, percent=nm_percent)
            self.cp_negatives['og_id']= -1
            self.bmp_negatives, self.bmp_num_col = transform_numeric_only(self.bmp_negatives, percent=nm_percent)
            self.bmp_negatives['og_id']= -2
            if not self.neg_fill.empty:
                self.neg_fill, self.rng_num_col = transform_numeric_only(self.neg_fill, percent=nm_percent)
                self.neg_fill['og_id']=-3
        else:
            if self.match_multiplier == 0:
                self.matches = transform_values(self.matches,severity=0.1, percent=0.05)
            
            self.matches['og_id']= og_id
            self.cp_negatives = transform_values(self.cp_negatives,severity=nm_severity,percent=nm_percent) #not sure if value transformation is needed -> needed to challenge algortihms
            self.cp_negatives['og_id']= -1
            self.bmp_negatives = transform_values(self.bmp_negatives,severity=nm_severity,percent=nm_percent)
            self.bmp_negatives['og_id']= -2
            if not self.neg_fill.empty:
                #og_id.extend(self.neg_fill['id'].to_list())
                self.neg_fill = transform_values(self.neg_fill,severity=nm_severity,percent=nm_percent)
                self.neg_fill['og_id']=-3
        
        # combine them to one dataframe
        if self.neg_fill.empty:
            ds_neg_lst = [self.cp_negatives,self.bmp_negatives]
        else:
            ds_neg_lst = [self.cp_negatives,self.bmp_negatives,self.neg_fill]
        
        self.neg_ds = pd.DataFrame(columns=self.matches.columns.to_list())
        for ds in ds_neg_lst:
            self.neg_ds = pd.concat([self.neg_ds,ds], sort=False)

        #TODO: match and non-match multiplier
        #somehow og_id is off, not sure how to handle it (?) - assert all(m['id'].isin(gen['og_id'])), "not all matches are in the generated dataset"
        if self.match_multiplier >0:
            m_copy = self.matches.copy()
            mc_og_id = m_copy['id'].to_list()
            for i in range(1,self.match_multiplier):
                m_copy = transform_values(m_copy,severity=nm_severity,percent=nm_percent)
                m_copy['og_id'] = mc_og_id
                self.matches = pd.concat([self.matches,m_copy],sort=False)
                i+=1
        
        assert all(self.matches['og_id'].isin(sampled_og_ds['id'])), "The OG_IDs of matches do not match with the IDs of sampled ds!"

        if self.negatives_multiplier>0:
            n_copy = self.neg_ds.copy()
            nc_og_ids = n_copy['og_id'].to_list()
            for i in range(1,self.negatives_multiplier):
                n_copy = transform_values(n_copy,severity=nm_severity,percent=nm_percent)
                n_copy['og_id'] = nc_og_ids
                self.neg_ds = pd.concat([self.neg_ds,n_copy],sort=False)
                i +=1
        
        assert not self.neg_ds[self.neg_ds['og_id']<0].empty, "the negative DF has wrong og_ids assigned" 

        gen_ds = pd.concat([self.matches,self.neg_ds],sort=False)
        
        #gen_ds['og_id'] = og_id

        #check uniqueness of ID and overwrite if not at 100% -> just introduce unique id (helps with labeling!)

        #How to handle ids if the sampled one is a string? Just cast str??

        #if not gen_ds['id'].is_unique:
        gen_ds['id'] = np.arange(len(gen_ds))
        
        assert len(gen_ds.index)==(len(self.matches.index)+len(self.neg_ds)), "The generated data set does not match the defined size!! ({} vs. {})".format(str(len(gen_ds.index)),str((match_size*self.match_multiplier)+(len(self.neg_ds.index)*self.negatives_multiplier)))
        #check if any match is in created negative data set
        self.check_matches_in_negatives()

        if is_string_dtype(sampled_og_ds['id']):
            enc = LabelEncoder()
            sampled_og_ds['id'] =enc.fit_transform(sampled_og_ds['id'])
            self.matches['og_id'] = enc.transform(self.matches['og_id'])
            gen_ds = pd.concat([self.matches,self.neg_ds],sort=False)
            gen_ds['id'] = np.arange(len(gen_ds))
        # profiling data creation
        print("\n--------------------------------------------------------------------------")
        print("\n Profiling of generated data set: \n")
        print("Generated records: {}".format(str(len(gen_ds.index))))
        print("Amount of Matches: {}".format(str(len(self.matches.index))))
        print("Amount of Non-Matches: {}".format(str(len(self.neg_ds.index))))
        print("Amount of randomly filled Non-Matches: {}".format(str(randomly_generated)))
        match_ratio = len(self.matches.index)/len(gen_ds.index)
        print("Match-ratio: {}".format(str(match_ratio)))
        print("Unique Entities: {}".format(str(dp.get_uniqueEntities_percentage(gen_ds))))
        print("-------------------------------------------------------------------------- \n")

        #self.matches = self.matches
        #self.cp_negatives =cp_negatives
        #self.bmp_negatives = bmp_negatives
        #self.neg_fill = neg_fill

        self.sampled_og_ds = sampled_og_ds
        self.gen_ds = gen_ds

        return sampled_og_ds,gen_ds
    
    def denoramlize(self):
        cols = self.sampled_og_ds.columns.to_list()
        cols.remove('id')
        random.shuffle(cols)
        self.ordered_denorm_cols_lst =cols

        self.sampled_og_ds, combined_col_a = ms.denormalize_DF(self.sampled_og_ds,cols_to_combine=cols,max_length=200)
        self.gen_ds, combined_col_b = ms.denormalize_DF(self.gen_ds,cols_to_combine=cols,max_length=200)

        assert combined_col_b==combined_col_a, "somehow not the same columns got put together"
        self.combined_col = combined_col_a
    
    def create_output_pairs(self,cosine_threshold:float):
        self.sampled_og_ds = self.sampled_og_ds.astype('object')
        self.gen_ds = self.gen_ds.astype('object')
        
        ws = sm.WhitespaceTokenizer()
        if not self.numeric:
            self.output_pair = ssj.cosine_join(self.sampled_og_ds,self.gen_ds, 'id','id',self.sampled_og_ds.columns[-1],self.gen_ds.columns[-1],ws,threshold=cosine_threshold,
                                        l_out_attrs=self.sampled_og_ds.columns.to_list(),
                                        r_out_attrs=self.gen_ds.columns.to_list())
        else:
            self.output_pair = ssj.cosine_join(self.sampled_og_ds,self.gen_ds, 'id','id',self.gen_bmp_join,self.gen_bmp_join,ws,threshold=cosine_threshold,
                                                l_out_attrs=self.sampled_og_ds.columns.to_list(),
                                                r_out_attrs=self.gen_ds.columns.to_list())
        return self.output_pair

    def manual_candidate_pair_creation(self):

        #copy needed Dataframes
        sampled = self.sampled_og_ds.copy()
        #consider new added ids after gen generation

        #filter_neg = 'og_id==-1 | og_id==-2 | og_id==-3'
        negatives = self.gen_ds[(self.gen_ds['og_id']==-1)|(self.gen_ds['og_id']==-2)|(self.gen_ds['og_id']==-3)]#self.gen_ds.query(filter_neg)
        matches = self.gen_ds.merge(negatives,how='left',indicator=True) # some how that will end up in a shape similiar of intent size
        matches = matches[matches['_merge']=='left_only'].drop(columns=['_merge'],axis=1)

        if not self.numeric:
            cols = self.ordered_denorm_cols_lst
            negatives, self.combined_col = ms.denormalize_DF(negatives,cols_to_combine=cols,max_length=200)
            sampled, self.combined_col = ms.denormalize_DF(sampled,cols_to_combine=cols,max_length=200)
            matches, self.combined_col = ms.denormalize_DF(matches,cols_to_combine=cols,max_length=200)
        
        sampled = sampled.add_prefix('l_')
        matches = matches.add_prefix('r_')
        negatives = negatives.add_prefix('r_')

        cp_matches = pd.merge(sampled, matches, how='outer',left_on='l_id',right_on='r_og_id',indicator=True)
        cp_matches = cp_matches[cp_matches['_merge']=='both']
        cp_matches.drop(columns=['_merge'],axis=1, inplace=True)
        cp_matches['gold']=1
        print("CP_matches filtered: {}".format(str(len(cp_matches.index))))

        try:
            cp_matches = uf.optimize_memory_usage(cp_matches,32,32)
        except:
            cp_matches =uf.optimize_memory_usage(cp_matches,8,8)

        cp_negatives = pd.merge(sampled,negatives,how='cross')
        cp_negatives.dropna(subset='l_id',inplace=True)
        cp_negatives.dropna(subset='r_id',inplace=True)

        try:
            cp_negatives = uf.optimize_memory_usage(cp_negatives,32,32)
        except:
            cp_negatives = uf.optimize_memory_usage(cp_negatives,8,8)

        print("Negatives outer length: {}".format(str(len(cp_negatives.index))))
        print(cp_negatives['r_og_id'].value_counts())

        negative_size = len(cp_matches.index)*self.non_match_factor
        try:
            unique_og_ids = len(self.matches.index)/self.match_multiplier
        except:
            unique_og_ids = len(self.matches.index)
        
        #negatives_cp = cp_negatives[cp_negatives['r_og_id']==-1]
        #negatives_bp = cp_negatives[cp_negatives['r_og_id']==-2]
        #negatives_rng = cp_negatives[cp_negatives['r_og_id']==-3]

        n_cp_m = cp_negatives[cp_negatives['l_id'].isin(matches['r_og_id']) & (cp_negatives['r_og_id']==-1)]
        n_bp_m = cp_negatives[cp_negatives['l_id'].isin(matches['r_og_id']) & (cp_negatives['r_og_id']==-2)]
        n_rng_m = cp_negatives[cp_negatives['l_id'].isin(matches['r_og_id'])&(cp_negatives['r_og_id']==-3)]

        nice_negatives = len(n_cp_m.index) + len(n_bp_m.index) + len(n_rng_m.index)

        final_negatives = pd.DataFrame(columns=cp_matches.columns.to_list())

        if nice_negatives == negative_size:
            final_negatives = pd.concat([n_cp_m,n_bp_m,n_rng_m],axis=0)
        elif nice_negatives < negative_size:
            final_negatives = pd.concat([n_cp_m,n_bp_m,n_rng_m],axis=0)
            
            diff_per_part = int((negative_size - nice_negatives)/3)

            cp_negatives.drop(n_cp_m.index,inplace=True)
            cp_negatives.drop(n_bp_m.index,inplace=True)
            cp_negatives.drop(n_rng_m.index,inplace=True)

            sampled_ncp = cp_negatives.sample(n=diff_per_part,replace=True)
            sampled_nbp = cp_negatives.sample(n=diff_per_part,replace=True)
            sampled_nrg = cp_negatives.sample(n=diff_per_part,replace=True)

            final_negatives = pd.concat([sampled_ncp,sampled_nbp,sampled_nrg],axis=0)
        else:
            if (len(n_cp_m.index)+len(n_bp_m.index)) == negative_size:
                final_negatives = pd.concat([n_cp_m,n_bp_m],axis=0)
            else:
                loops = math.ceil(negative_size/unique_og_ids)

                ncp = pd.DataFrame(columns=cp_negatives.columns)
                nbp = pd.DataFrame(columns=cp_negatives.columns)
                nrg = pd.DataFrame(columns=cp_negatives.columns)

                for i in range(1,loops):
                    
                    if not cp_negatives[cp_negatives['r_og_id']==-1].empty:
                        ncp = cp_negatives[cp_negatives['r_og_id']==-1].drop_duplicates(subset='l_id',ignore_index=False)
                    if not cp_negatives[cp_negatives['r_og_id']==-2].empty:
                        nbp = cp_negatives[cp_negatives['r_og_id']==-2].drop_duplicates(subset='l_id',ignore_index=False)
                    if not cp_negatives[cp_negatives['r_og_id']==-3].empty:
                        nrg = cp_negatives[cp_negatives['r_og_id']==-3].drop_duplicates(subset='l_id',ignore_index=False)
                    if final_negatives.empty:
                        try:
                            final_negatives = pd.concat([ncp,nbp,nrg],axis=0)
                        except:
                            final_negatives =pd.concat([ncp,nrg],axis=0)
                    else:
                        final_negatives = pd.concat([final_negatives,ncp,nbp,nrg],axis=0)
                    
                    if not ncp.empty:
                        cp_negatives.drop(ncp.index,inplace=True)
                    if not nbp.empty:
                        cp_negatives.drop(nbp.index,inplace=True)
                    if not nrg.empty:
                        cp_negatives.drop(nrg.index, inplace=True)
                
        if len(final_negatives.index) > negative_size:
            surplus = len(final_negatives.index)-negative_size
            final_negatives = final_negatives.iloc[:-surplus,:]
        
        final_negatives['gold'] = 0      

        self.output_pair = pd.concat([cp_matches,final_negatives],axis=0,ignore_index=True)
        self.output_pair = self.output_pair.reset_index()
        #self.output_pair.insert(loc=0,column='_id',value=np.arange(len(self.output_pair)))
        self.output_pair['_id'] = np.arange(len(self.output_pair))
        
        #move '_id' to first column index so that gold will be last one!
        h_col = self.output_pair.columns.tolist()
        h_col = h_col[-1:] + h_col[:-1]
        self.output_pair = self.output_pair[h_col]
        print(self.output_pair.columns)
        return self.output_pair

    def add_missing_matches(self,ds:pd.DataFrame):

        match_in_op = self.output_pair[self.output_pair['gold']==1]
        """if self.match_multiplier==0:
            match_in_op = match_in_op.drop_duplicates(subset=['r_id'])
        else:
            match_in_op = match_in_op.drop_duplicates(subset=['r_og_id'])"""
        match_in_op = len(match_in_op.index)
        match_goal = len(self.matches.index)
        match_diff = match_goal-match_in_op

        try:
            check = self.matches.merge(self.output_pair,how='outer',left_on='og_id',right_on='r_og_id',indicator=True)
        except:
            self.matches['og_id'] = self.matches['id'].astype('object')
            check = self.matches.merge(self.output_pair,how='outer',left_on='og_id',right_on='r_og_id',indicator=True)

        missing_matches=check[check['_merge']=='left_only']
        missing_matches.drop(columns=['_merge'],axis=1,inplace=True)
        missing_matches = missing_matches[self.matches.columns.to_list()]


        assert not any(missing_matches['og_id'].isin(self.output_pair['r_og_id'])), "Some missing matches are in outputpair!"
        assert all(missing_matches['og_id'].isin(self.matches['og_id'])), "Some missing matches are not in created matches!"
        
        if match_diff < len(missing_matches.index):
            warnings.warn("Missing matches DataFrame is too big for the needed Difference in the output pairs!")
        if match_diff > len(missing_matches.index):
            warnings.warn("Missing matches DataFrame is too small for the needed Difference in the output pairs!")
        if missing_matches.empty:
            print("No missing matches in outputpair detected!")
            return self.output_pair
        
        #get denormalized column for missing matches
        if not self.numeric:
            cols = self.ordered_denorm_cols_lst
            missing_matches, self.combined_col = ms.denormalize_DF(missing_matches,cols_to_combine=cols,max_length=200)

        missing_matches= missing_matches.add_prefix('r_')
        
        find_in_sample = self.sampled_og_ds.merge(missing_matches,how='left',left_on='id',right_on='r_og_id',indicator=True)
        
        find_in_sample = find_in_sample[find_in_sample['_merge']=='both']
        find_in_sample.drop(columns=['_merge'],axis=1,inplace=True)
        find_in_sample = find_in_sample[self.sampled_og_ds.columns.to_list()]
        find_in_sample = find_in_sample.add_prefix('l_')
        
        if self.match_multiplier==0:
            missing_op = find_in_sample.merge(missing_matches,how='inner',left_on='l_id',right_on='r_og_id')
        else:
           missing_op = find_in_sample.merge(missing_matches,how='outer',left_on='l_id',right_on='r_og_id',indicator=True)
           missing_op = missing_op[missing_op['_merge']=='both']
           missing_op.drop(columns=['_merge'],axis=1,inplace=True)
        # some how missing_matches is always 1 smaller than match_diff, not sure why though
        #assert len(missing_op.index)/self.match_multiplier <= match_diff, "merge of find_in_sample and missing_matches results in a size bigger than match_diff ({} vs {})".format(str(len(missing_op.index)/self.match_multiplier),str(match_diff))

        missing_op['gold'] = 1
        og_id = missing_op['r_og_id']
        #r_id = missing_matches['id'] # not sure if that works
        # need also the right r_ids
        try:
            missing_op = missing_op.merge(self.gen_ds, how='outer',left_on='r_id',right_on='id',indicator=True)
        except:
            missing_op['r_id'] = missing_op['r_id'].astype('object')
            missing_op = missing_op.merge(self.gen_ds, how='outer',left_on='r_id',right_on='id',indicator=True)
            
        missing_op = missing_op[missing_op['_merge']=='both']
        missing_op.drop(columns=['_merge'],axis=1, inplace=True)
        missing_op['r_id'] = missing_op['id']
        missing_op.drop(columns=self.gen_ds.columns.to_list(),axis=1,inplace=True)

        #assert len(missing_op.index) <= match_diff, "merge of missing op and gen_ds results in a size bigger than match_diff ({} vs {})".format(str(len(missing_op.index)),str(match_diff))

        missing_op['r_og_id'] = og_id
        missing_op['gold'] = 1
        ds = pd.concat((ds,missing_op),axis=0,ignore_index=True) #ds.append(missing_op)
        ds = ds.drop_duplicates()
        #reset ids to also includ the added missing matches (should not cause any trouble)
        ds.reset_index(inplace=True)
        ds['_id'] = ds.index
        ds.drop(columns=['index'],axis=1)
        self.output_pair = ds

        print("{} overall candidate pairs".format(str(len(self.output_pair.index))))
        print("{} matching candidate pairs added".format(str(len(missing_op.index))))

        return ds



    
    def run(self, filename:str, output_path:str, entities:int, nm_factor:int =3,nm_severity:float =0.3,nm_percent:float=0.1,
            match_multiplier:int =4, negatives_multiplier:int =3,numeric:bool =False):

        og_name = filename +'_og'
        gen_name =filename +'_gen'
        op_name = filename +'_op'

        self.numeric = numeric
        self.match_multiplier = match_multiplier
        self.negatives_multiplier = negatives_multiplier
        #self.nm_severity = nm_severity
        #self.nm_percent = nm_percent
        
        og,gen = self.create_data(entities,nm_factor,nm_severity, nm_percent)

        og.to_csv(output_path+og_name+'.csv',index=False)
        gen.to_csv(output_path+gen_name+'.csv',index=False)

        if not self.numeric:
            self.denoramlize()
            uf.write_strc_nstrc_to_csv(og,self.get_combined_col(),og_name,output_path)
            uf.write_strc_nstrc_to_csv(gen,self.get_combined_col(),gen_name,output_path)
        else:
            uf.write_no_num_to_csv(og,self.m_num_col,og_name,output_path)
            uf.write_no_num_to_csv(gen,self.m_num_col,gen_name,output_path)

        m = self.get_matches()
        help_num = self.gen_size/(nm_factor+1)
        if self.match_multiplier >0:
            #assert len(m.index)== help_num*self.match_multiplier, "matches before outputpair creation are off :D ({} vs. {})".format(str(len(m.index)),str(help_num*self.match_multiplier))
            assert all(m['og_id'].isin(gen['og_id'])), "not all matches are in the generated dataset"
        else:
            assert len(m.index)==help_num, "matches before outputpair creation are off :D"
            assert all(m['id'].isin(gen['og_id'])), "not all matches are in the generated dataset"

        #the next statement reduces the matches, not sure why though...
        output_pairs = self.manual_candidate_pair_creation()
        if not all(m['id'].isin(output_pairs['r_og_id'])):
            warnings.warn("Not all matches are in the candidate_pairs",UserWarning)
        
        dp.show_labeling_results(m,output_pairs)

        if not self.numeric:
            uf.write_strc_nstrc_to_csv(output_pairs,self.get_combined_col(),op_name,output_path)
        else:
            num_cols = ["l_" + match for match in self.m_num_col]
            num_cols.extend(["r_"+ match for match in self.m_num_col])
            uf.write_no_num_to_csv(output_pairs,num_cols,op_name,output_path)

        print(output_pairs['gold'].value_counts())

        train, validate, test = self.train_validate_test_split('gold',filename,output_path)

        print("\nMatch-ratio")
        print("train: {}".format(str(dp.get_match_ratio(train,'gold'))))
        print("validate: {}".format(str(dp.get_match_ratio(validate,'gold'))))
        print("test: {}".format(str(dp.get_match_ratio(test,'gold'))))

        if not self.numeric:
            train_strc,train_nstrc = uf.separate_into_strc_nstrc(train,nstr_col=self.get_combined_col())
            validate_strc, validate_nstrc = uf.separate_into_strc_nstrc(validate, nstr_col=self.get_combined_col())
            test_strc,test_nstrc =uf.separate_into_strc_nstrc(test,nstr_col=self.get_combined_col())

            train_strc.to_csv(output_path + filename +'_train_strc.csv',index=False)
            train_nstrc.to_csv(output_path + filename +'_train_nstrc.csv',index=False)

            validate_strc.to_csv(output_path + filename +'_validate_strc.csv',index=False)
            validate_nstrc.to_csv(output_path + filename +'_validate_nstrc.csv',index=False)

            test_strc.to_csv(output_path+ filename +'_test_strc.csv',index=False)
            test_nstrc.to_csv(output_path + filename +'_test_nstrc.csv',index=False)
        else:
            uf.write_no_num_to_csv(train,num_cols,filename + '_train',output_path)
            uf.write_no_num_to_csv(validate,num_cols,filename+'_validate',output_path)
            uf.write_no_num_to_csv(test,num_cols,filename+'_test',output_path)

    def get_matches(self):
        return self.matches
    def get_output_pairs(self):
        return self.output_pair
    def get_og_sample(self):
        return self.sampled_og_ds
    def get_combined_col(self):
        return self.combined_col
    def get_gen_ds(self):
        return self.gen_ds
    def get_bmp_negatives(self):
        return self.bmp_negatives
    def get_neg_fill(self):
        return self.neg_fill
    def get_cp_negatives(self):
        return self.cp_negatives
    def get_negatives_ds(self):
        return self.neg_ds
    
    def set_numeric(self, num:bool):
        self.numeric = num
    def set_match_multiplier(self, mm:int):
        self.match_multiplier = mm
    def set_negatives_mulitplier(self, nm:int):
        self.negatives_multiplier =nm