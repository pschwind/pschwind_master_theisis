"""
create function able to load all common file formats into a pandas dataframe

- csv
- json
- html

"""

import pandas as pd
import os
import numpy as np
import json
import csv
import gzip
import shutil


csv_identifier ='.csv'
json_identifier = '.json'
html_identifier = '.html'

def load_dataset (path: str) -> pd.DataFrame :
    """
    Enable to load most common data set formats into a dataframe.
    Args:
        - path: String to locate where file is stored. Needs relative path!
    returns:
        - DataFrame, which probably needs some more adjustments (delimiter, etc.)
    """
    if csv_identifier in path:
        #do pd.read_csv
        df = pd.read_csv(path)
    elif json_identifier in path:
        # do some json reading
        df = pd.read_json(path, lines= True)
    elif html_identifier in path:
        #do html reading method
        df = pd.read_html(path)
    else:
        raise Exception('load_dataset is only able to load csv, json and html formats. Please make sure to only use these formats')
    return df

def get_csv_file_dict(path):
    """
    Function to enable use of Strudel. Strudel needs a json_file_dict to generate 
    the csv structure.
    Args:
        - path to dataset
    returns: json_file_dict
    """
    csv_file_dicts = {}
    if csv_identifier in path:
        with open(path, encoding="utf8") as ds_csv_file:
            csvReader = csv.DictReader(ds_csv_file)
            for rows in csvReader:
                id = rows['id']
                csv_file_dicts[id] = rows

    return csv_file_dicts

def create_file_jl_gzip (path: str):
    file_no_type = path.split('.',1)[0]
    new_path = str(file_no_type + '.jl')
    shutil.copyfile(path,new_path)
    
    with open(new_path, 'rb') as f_in, gzip.open(str(new_path + '.gz'), 'wb') as f_out:
        f_out.writelines(f_in)

file_path = r'data_set\acm_demo.csv'
create_file_jl_gzip(file_path)

