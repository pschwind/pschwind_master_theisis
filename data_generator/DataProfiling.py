import pandas as pd
import numpy as np

from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_extraction.text import TfidfVectorizer

from data_generator.ManageStructure import transfromDataType



# experiment with standard data profiling methods of pandas and common static sense
# print(ds.describe())
# print(ds.columns)
# print(ds.info())


# storing the formats of the columns for unstructuring them
# One other item I want to highlight is that the object data type can actually contain multiple different types.
# ToDo: quantify strucuture of data

# one way could be via the ratio of object dtypes in the data set

# ToDo: create return values as in ManageStructure.py!

def ratio_object_dtypes(ds: pd.DataFrame) ->float:
    """
    function to understand how strucutred the passed data set is. Close to 0 means high structure, close to 1 means
    low structure (indicates). Why? Data types are important for structure, if they are in form of pandas object,
    that means it can be either string, int, float, etc. Basically all dtypes => if data set has high amount of object
    dtypes means high unstrucuturedness (?)
    :param ds: preferable pd.DataFrame
    :return: ratio of dtypes objects in data set
    """
    if not isinstance(ds, pd.DataFrame):
        raise TypeError
    

    df_format = ds.dtypes.value_counts()
    other = 0
    obj = 0
    for i in df_format.index:
        if i == 'object':
            obj = df_format[i]
        else:
            other += df_format[i]
    ratio = obj / (obj + other)
    return ratio


# ratio = ratio_object_dtypes(ds)
# print(ratio)

# report = ds.profile_report()
# report.to_file('profile.html')

# ToDo: create return values as in ManageStructure.py!


def get_nullValues_percentage(ds: pd.DataFrame) ->float:
    """
    Is standard statistic of data profiling. Shows how good the quality of the data set is.
    :param ds: 
    :return: percentage of null values in data set 
    """
    if not isinstance(ds, pd.DataFrame):
        raise TypeError
    nv_rows = ds.isnull().sum(axis=0)
    nv_count = nv_rows.sum()
    ds_size = ds.size

    nv_percentage = nv_count/ds_size

    return nv_percentage

# ToDo: create return values as in ManageStructure.py!


def get_uniqueEntities_percentage (ds: pd.DataFrame) -> float:
    """
    show percentage of unique values -> standard measurement for data profiling.
    result: 1 means all rows are unique
    result: 0 means dataframe solely consists of duplicates

    :param ds: pd.DataFrame
    :return: percentage of unique values
    """
    if not isinstance(ds, pd.DataFrame):
        raise TypeError
    #print(ds)
    # cannot use drop_duplicates() when having lists -> work around using dtype string
    try:
        unique_entities = ds.drop_duplicates()
    except:
        unique_entities= ds.loc[ds.astype(str).drop_duplicates().index]
        
    ue_percentage = len(unique_entities)/len(ds)
    return ue_percentage

#print(get_uniqueEntities_percentage(ue_test))
#print(get_uniqueEntities_percentage(ds))
# ToDo: measure decivisness of nummeric attributes
# ToDo: create an own specific data profiling data frame with information you need
# ToDo: create important feature attribute function
# ToDo: create function that determines the type of the feature/attributes
# ToDo: create density function to enable to show density of a feature/attribute
def get_Dtype_diversity(ds:pd.DataFrame):
    """
    Function to get the number of different Dtypes of a data set.
    It determines how structured a dataset is. Low number means
    less structure
    Arg:
        - ds: pd.DataFrame
    Returns:
        - amount of different data types.
    """
    info = ds.dtypes.value_counts()
    diversity = len(info)
    return diversity



def create_profile(ds:pd.DataFrame):
    dtype_div = get_Dtype_diversity(ds=ds)
    unique_ent = get_uniqueEntities_percentage(ds=ds)
    nullVal = get_nullValues_percentage(ds=ds)
    obj_ratio = ratio_object_dtypes(ds=ds)

    ds_profile = [['DataType_Diversity', dtype_div], ['Percentage of Unique Entities', unique_ent],['Percentage of Null Values', nullVal],
                 ['Percentage of Dtypes object', obj_ratio]]
    
    return ds_profile


def attributes_profile(ds: pd.DataFrame):
    """
    Function to display information about the data itself. While create_profile() is looking at the overall DataFrame this function
    will concentrate on the attributes to identify potential keys, etc. 
    """

    lst_col = ds.columns.values.tolist()

    unique_vals = 0
    missing_vals = 0

    prof = pd.DataFrame(data=0, index=range(0,len(lst_col)), columns=['col_name','unique_values', 'uv_percentage', 'missing_values', 'mv_percentage'])
    prof['col_name'] = lst_col

    for idx, cols in prof.iterrows():

        col_name = prof.at[idx,'col_name']

        unique_vals = ds[col_name].nunique()
        missing_vals = ds[col_name].isnull().sum()

        prof.at[idx,'unique_values'] = unique_vals
        prof.at[idx,'uv_percentage'] = unique_vals/(len(ds[col_name]))
        prof.at[idx,'missing_values'] = missing_vals
        prof.at[idx,'mv_percentage'] = missing_vals/(len(ds[col_name]))
    
    print(prof)


def get_match_ratio(ds:pd.DataFrame, label:str):
    """
    Analyse the ratio of entities that are matches in a dataframe.
    Args:
        - ds: Dataframe to check
        - label: String to identify column to check
    return:
        - percentage of matches in data set
    """
    ds_len = len(ds.index)
    matches = len(ds[ds[label]==1].index)

    return matches/ds_len

def sim_score_deviation(ds:pd.DataFrame,col_name:str):
    print("The standard deviation of {} is: {}".format(col_name,str(ds[col_name].std())))
    print("The mean of {} is: {}".format(col_name,str(ds[col_name].mean()))) 
    print("The median of {} is: {}".format(col_name,str(ds[col_name].median())))

def show_labeling_results(matches:pd.DataFrame, op:pd.DataFrame):

    all_matches = op[op['gold']==1]
    goal = len(matches.index)
    total_matches = len(all_matches.index)
    correctly_labeled = len(all_matches[all_matches['r_og_id'].isin(matches['og_id'])].index)
    wrongly_labeled = len(all_matches[~all_matches['r_og_id'].isin(matches['og_id'])].index)
    matches_missing_unique = len(matches[~matches['og_id'].isin(op['r_og_id'])].index)
    matches_missing_total = goal - total_matches
    if matches_missing_unique!=0:
        match_missing_multiplier = matches_missing_total / matches_missing_unique
    else:
        match_missing_multiplier = 0
    
    print("--------------------------------------------------------------------------------")
    print("Labeling Results new:\n")
    print("Goal of matches: {}".format(str(goal)))
    print("Total matches: {}".format(str(total_matches)))
    print("Matches labeled correctly: {}".format(str(correctly_labeled)))
    print("Wrong labeled matches: {}".format(str(wrongly_labeled)))
    print("Total missing matches: {}".format(str(matches_missing_total)))
    print("Uniquely missing matches: {}".format(str(matches_missing_unique)))
    print("Factor of missing matches: {}".format(str(match_missing_multiplier)))
    


    print("--------------------------------------------------------------------------------")
    print("Labeling Results old:\n")
    check = pd.merge(matches,op, how='left',left_on='og_id',right_on='r_og_id',indicator=True)
    check = check[check['_merge']=='left_only']
    print("Missing matches in output_pairs: "+str(len(check.index)))
    check = check.drop_duplicates(subset='id')
    print("Unique missing match ids: " +str(len(check.index)))
    print("Goal of matches: "+ str(len(matches.index)))
    try:
        print("Labeled as matches: {}".format(str(op['gold'].value_counts()[1])))
    except:
        print("Labeled as matches: 0")
        print("Label Distribution: \n {}".format(str(op['gold'].value_counts())))
    check2 = op[op['gold']==1]
    check2 = check2[~check2['r_og_id'].isin(matches['og_id'])]
    print("Wrongly labeled matches: "+str(len(check2.index)))
    check3 = op[op['gold']==1]
    check3 = check3[check3['r_og_id'].isin(matches['og_id'])]
    print("Matches labeled as matches: "+str(len(check3.index)))
    check3 = check3.drop_duplicates(subset="r_og_id")
    print("Unique matches labeled as matches: " +str(len(check3.index)))
    print("\n--------------------------------------------------------------------------------")

    def creation_info(gen:pd.DataFrame):

        print("very usefull information to understand how many hard negatives their are")