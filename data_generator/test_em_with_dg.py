"""
This file is used to test out the experiment setup of this thesis.

Expectations:
- structure manipulation of data sets works
- manipulation of numeric attribute decisiveness works
- entity matching with dataset does make sense

Test scenarios:
- Entity Matching with a high structured data sets
- Entity Matching with no structured data sets
- Entity Matching with numeric attribute decisiveness
- Entity Matching with string attribute dominant matching

Current focus on DeepMatcher

"""

#----------------------------------------------------------------------------------------
#
#
# General EM-process:
# 1. load and generate data
# 2. preprocess data
# 3. Blocking 
# 4. Create candidate pairs
# 5. Create goldstandard
# 5. Create train, val, test split
# 6. Learn and apply model
# 7. save and log results of model
#
#
#---------------------------------------------------------------------------------------

import pandas as pd
import numpy as np
import sys
import os
import random

import py_entitymatching as em
import py_stringmatching as sm
import py_stringsimjoin as ssj
import deepmatcher as dm

from data_generator import DataGenerator as dg
from data_generator import ManageStructure as ms

with open('DeepLearning/logs/em_test_stdout.txt', 'w') as sys.stdout:
    aws = pd.read_csv('data_set/Amazon-GoogleProducts/Amazon.csv',encoding='unicode_escape')
    col_list =aws.columns.to_list()

    ent = len(aws.index)

    aws_strc = dg.generate_data(aws,max_length=256,entities=ent)

    ns_col_list = aws.columns.to_list()
    ns_col_list.remove('id')

    # for DeepMatcher the shuffling of the columns has to be same in order to work or rename column

    random.shuffle(ns_col_list)

    aws_no_strc = ms.denormalize_DF(aws_strc, cols_to_combine=ns_col_list, max_length=256, shuffle=False)
    aws_og_no_strc = ms.denormalize_DF(aws, cols_to_combine=ns_col_list, max_length=256, shuffle=False)


    aws = aws.add_prefix('og.')
    aws_strc = aws_strc.add_prefix('str.')

    aws_no_strc = aws_no_strc.add_prefix('nstr.')
    aws_og_no_strc = aws_og_no_strc.add_prefix('og_nstr.')

    aws_strc.to_csv('data_set/experiment_runs/aws_struct.csv')
    aws_no_strc.to_csv('data_set/experiment_runs/aws_no_struct.csv')
    aws_og_no_strc.to_csv('data_set/experiment_runs/aws_og_no_strc.csv')

    print(ssj.profile_table_for_join(aws))
    print(ssj.profile_table_for_join(aws_strc))
    print(ssj.profile_table_for_join(aws_no_strc))
    print(ssj.profile_table_for_join(aws_og_no_strc))

    #AssertionError: join attribute 'og.title' in left table is not of string type. -> transform them to object type
    aws = aws.astype('object')
    aws_strc = aws_strc.astype('object')
    aws_no_strc = aws_no_strc.astype('object')
    aws_og_no_strc = aws_og_no_strc.astype('object')

    ws = sm.WhitespaceTokenizer()

    #unfortunately does not have a ignore column area and one is not able to join with the og_id
    #TODO: is this already blocking??

    print("\n Create structured outputpairs \n")
    op_aws_strc = ssj.cosine_join(aws, aws_strc,'og.id','str.id', 'og.name','str.name', ws , 0.7,
                                    l_out_attrs=['og.name','og.description','og.manufacturer','og.price'],
                                    r_out_attrs=['str.name','str.description','str.manufacturer','str.price'],
                                    show_progress=True)


    #select the right matches for structure

    for index, rows in op_aws_strc.iterrows():
        aws_id= rows['l_og.id']
        strc_id = rows['r_str.id']

        for idx, r in aws_strc.iterrows():
            
            if r['str.og_id'] == aws_id and r['str.id']==strc_id:
                op_aws_strc.at[index, '_sim_score'] = 1 
                break
            else:
                op_aws_strc.at[index, '_sim_score'] = 0
        


    op_aws_strc = op_aws_strc.rename(columns={'_sim_score': 'gold'})
    op_aws_strc['gold'] = op_aws_strc['gold'].astype('int')
    op_aws_strc = op_aws_strc.set_index('_id')
    op_aws_strc.to_csv('data_set/experiment_runs/outputpairs_aws_strc.csv')


    #split set into train, validate and test for structure
    train_aws_strc, validate_aws_strc, test_aws_strc = \
                np.split(op_aws_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_aws_strc)), int(.8*len(op_aws_strc))])





    # make them to csv so DeepMatcher can use them

    train_aws_strc.to_csv('data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/train_aws_strc.csv')
    validate_aws_strc.to_csv('data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/validate_aws_strc.csv')
    test_aws_strc.to_csv('data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/test_aws_strc.csv')


    # no structure run

    #TODO: 
    #need to get for unstructure part the newly created column to join on
    #I believe it is always the last column, but not sure though

    print("\n Create no structured outputpairs \n")
    op_aws_no_strc = ssj.cosine_join(aws_og_no_strc, aws_no_strc,'og_nstr.id','nstr.id', aws_og_no_strc.columns[-1],aws_no_strc.columns[-1], ws , 0.5,
                                    l_out_attrs=['og_nstr.id',aws_og_no_strc.columns[-1]],
                                    r_out_attrs=['nstr.id',aws_no_strc.columns[-1]],
                                    show_progress=True)

    #create gs for no_struct_aws (should be able to copy and adapt the one of structured)
    for index, rows in op_aws_no_strc.iterrows():
        aws_nstr_id= rows['l_og_nstr.id']
        nstrc_id = rows['r_nstr.id']

        for idx, r in aws_no_strc.iterrows():
            
            if r['nstr.og_id'] == aws_nstr_id and r['nstr.id']==nstrc_id:
                op_aws_no_strc.at[index, '_sim_score'] = 1 
                break
            else:
                op_aws_no_strc.at[index, '_sim_score'] = 0
        


    op_aws_no_strc = op_aws_no_strc.rename(columns={'_sim_score': 'gold'})
    op_aws_no_strc['gold'] = op_aws_no_strc['gold'].astype('int')
    op_aws_no_strc = op_aws_no_strc.set_index('_id')
    op_aws_no_strc.to_csv('data_set/experiment_runs/outputpairs_aws_no_strc.csv')

    #split no struct aws into train, validation and test set
    train_aws_nstrc, validate_aws_nstrc, test_aws_nstrc = \
                np.split(op_aws_no_strc.sample(frac=1, random_state=42), 
                        [int(.6*len(op_aws_no_strc)), int(.8*len(op_aws_no_strc))])

    train_aws_nstrc.to_csv('data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/train_aws_nstrc.csv')
    validate_aws_nstrc.to_csv('data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/validate_aws_nstrc.csv')
    test_aws_nstrc.to_csv('data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/test_aws_nstrc.csv')

    #DeepMatcher for structure



    # load data via DeepMatcher
    print("\n Start DeepMatcher for structure \n")
    train, validation, test = dm.data.process(
        path='data_set/Amazon-GoogleProducts/DeepMatcher/EM_test',
        train='train_aws_strc.csv', 
        validation='validate_aws_strc.csv',
        test='test_aws_strc.csv',
        left_prefix='l_og.',
        right_prefix='r_str.',
        label_attr ='gold',
        id_attr='_id')
    
    
    summarizer =['sif', 'rnn', 'attention','hybrid']
    for sum in summarizer:

        model = dm.MatchingModel(attr_summarizer=sum)

        #model.initialize(train)
        model.run_train(
            train,
            validation,
            epochs=25,
            learning_rate=1e-3,
            batch_size=32,
            best_save_path='data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/best_save_path.pth')

        pred = model.run_eval(test)

        print("Best F1-score of {} during test was: {}".format(sum, pred))

    #delete cachedata.pth so it does not f up the script
    print("\n DeepMatcher ended for structure \n")
    print("\n Delete chachedata.pth for unstructured run ")
    if os.path.exists('data_set/Amazon-GoogleProducts/DeepMatcher/cacheddata.pth'):
        os.remove('../data_set/Amazon-GoogleProducts/DeepMatcher/cacheddata.pth')
          
   # unstructured run
    print("\n Start DeepMatcher for no structure \n")
    train_ns, validation_ns, test_ns = dm.data.process(
        path='../data_set/Amazon-GoogleProducts/DeepMatcher/EM_test',
        train='train_aws_nstrc.csv', 
        validation='validate_aws_nstrc.csv',
        test='test_aws_nstrc.csv',
        left_prefix='l_og_nstr.',
        right_prefix='r_nstr.',
        label_attr ='gold',
        id_attr='_id')

    #TODO: Understand the mistake here seems that there is some prefix issue. I think!
    #ValueError: Attribute l_og_nstr.id is not a left or a right table column, not a label or id and is not ignored. Not sure what it is...

    summarizer =['sif', 'rnn', 'attention','hybrid']
    for sum in summarizer:

        model = dm.MatchingModel(attr_summarizer=sum)

        #model.initialize(train)
        model.run_train(
            train_ns,
            validation_ns,
            epochs=25,
            learning_rate=1e-3,
            batch_size=32,
            best_save_path='../data_set/Amazon-GoogleProducts/DeepMatcher/EM_test/best_save_path_nstrc.pth')

        pred = model.run_eval(test_ns)

        print("Best F1-score of {} during test was: {}".format(sum, pred))
    print("DeepMatcher ended for no structure \n") 

